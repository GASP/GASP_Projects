# GASP games Snake Sheet
# Written by Richard Martinez

from gasp import games
from gasp import boards
from gasp import color
from random import randint

BOX_SIZE = 40
MARGIN = 60
SNAKE_COLOR = color.RED
EMPTY_COLOR = color.LIGHTGRAY


class SnakeBox(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.is_snake = 0
        self.is_food = 0
        self.food = None

    def snakify(self):
        self.is_snake = 1
        self.set_color(SNAKE_COLOR)

    def unsnakify(self):
        self.is_snake = 0
        self.set_color(EMPTY_COLOR)

    def foodify(self):
        self.is_food = 1
        self.food = Food(self.screen, self.screen_x, self.screen_y)

    def eat(self):
        self.is_food = 0
        self.food.destroy()
        self.board.new_food()


class SnakeBoard(boards.SingleBoard):
    def __init__(self, n_cols, n_rows, snake_size, interval):
        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, BOX_SIZE)
        self.set_background_color(EMPTY_COLOR)
        self.game_over = 0

        # This is what allows for the snake to wrap around the edge of the screen
        self.create_directions(orthogonal_only=1, wrap=1)

        # Get the center of the board and a random direction to start moving
        i = n_cols // 2  # Integer Division
        j = n_rows // 2
        direction = boards.random_direction(orthogonal_only=1)
        self.snake = Snake(self, self.grid[i][j], direction)

        self.snake_size = snake_size
        self.interval = interval
        self.tick_count = 0

        # Extend the snake in the opposite direction than it is facing
        snake_box = self.grid[i][j]
        direction = boards.turn_180(direction)
        for n in range(1, snake_size):
            snake_box = snake_box.direction[direction]
            self.snake.extend(snake_box)

        # Map the arrow keys to the boards directions
        self.key_movements = {
            boards.K_UP: boards.UP,
            boards.K_DOWN: boards.DOWN,
            boards.K_LEFT: boards.LEFT,
            boards.K_RIGHT: boards.RIGHT
        }

        # Place the first food
        self.new_food()

    def keypress(self, key):
        if self.game_over:
            return

        # Handle arrow keys (pass if non-arrow key)
        try:
            direction = self.key_movements[key]
        except KeyError:
            return

        # Prevent the snake from being able to kill itself by turning 180 degrees
        if direction == boards.turn_180(self.snake.direction):
            return

        self.snake.set_direction(direction)

    def new_gamecell(self, i, j):
        return SnakeBox(self, i, j)

    def tick(self):
        if self.game_over:
            # Display the lose text
            score = f"You lost! Score: {len(self.snake.position) + 1}"
            games.Text(self.screen, 720//2, 720//2, score, size=35, color=color.BLUE)
            return

        # Move the snake according to its interval
        self.tick_count += 1
        if self.tick_count == self.interval:
            self.tick_count = 0
            if not self.snake.move():
                self.game_over = 1

        # Outlines:
        # Only draw the outlines of the snake:
        self.limit_outlines(lambda box: box.is_snake, color.BLACK, EMPTY_COLOR)

        # Draw all outlines:
        # self.draw_all_outlines()

    def new_food(self):
        while True:  # Keep going until found a safe spot
            i = randint(0, self._n_cols - 1)  # Pick coords
            j = randint(0, self._n_rows - 1)
            if not self.grid[i][j].is_snake:  # Is this space currently occupied?
                break  # No? Great!
        self.grid[i][j].foodify()  # Foodify that spot


class Snake:
    def __init__(self, board, box, direction):
        self.position = []
        self.position.append(box)
        self.direction = direction
        self.board = board

        box.snakify()

    def move(self):
        first = self.position[0].direction[self.direction]  # Grab the head of the snake
        if first.is_food:
            first.eat()
        else:
            self.position.pop().unsnakify()

        if first.is_snake:
            return 0  # You lose

        first.snakify()
        self.position.insert(0, first)
        return 1

    def extend(self, box):
        self.position.append(box)
        box.snakify()

    def set_direction(self, direction):
        self.direction = direction


class Food(games.Circle):
    def __init__(self, screen, x, y):
        self.init_circle(screen,
                         x + BOX_SIZE/2,
                         y + BOX_SIZE/2,
                         radius=BOX_SIZE/2 - 2,
                         color=color.GREEN)


snakeGame = SnakeBoard(15, 15, 3, 10)
snakeGame.mainloop()
