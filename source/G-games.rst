..
    Copyright Gareth McCaughan and Jeffrey Elkner. All rights reserved.
    
    CONDITIONS:

    A "Transparent" form of a document means a machine-readable form,
    represented in a format whose specification is available to the general
    public, whose contents can be viewed and edited directly and
    straightforwardly with generic text editors or (for images composed of
    pixels) generic paint programs or (for drawings) some widely available
    drawing editor, and that is suitable for input to text formatters or for
    automatic translation to a variety of formats suitable for input to text
    formatters. A copy made in an otherwise Transparent file format whose
    markup has been designed to thwart or discourage subsequent modification
    by readers is not Transparent. A form that is not Transparent is
    called "Opaque". 

    Examples of Transparent formats include LaTeX source and plain text.
    Examples of Opaque formats include PDF and Postscript.  Paper copies of
    a document are considered to be Opaque.

    Redistribution and use of this document in Transparent and Opaque
    forms, with or without modification, are permitted provided that the
    following conditions are met: 

    - Redistributions of this document in Transparent form must retain
      the above copyright notice, this list of conditions and the following
      disclaimer. 

    - Redistributions of this document in Opaque form must reproduce the
      above copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided with
      the distribution, and reproduce the above copyright notice in the
      Opaque document itself.

    - Neither the name of Scripture Union, nor LiveWires nor the names of
      its contributors may be used to endorse or promote products derived
      from this document without specific prior written permission. 

    DISCLAIMER:

    THIS DOCUMENT IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS,
    CONTRIBUTORS OR SCRIPTURE UNION BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS DOCUMENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Graphics API for Students of Python: GASP - Games Module
=========================================================


Introduction
------------

This sheet describes briefly the GASP games module.

The games module allows for more complex games to be created with moving parts
such as sprites and sounds.

This sheet is more of a reference than an introduction. If you get confused,
ask a teacher for help. If you *are* a teacher or you are studying this on your own
and it still doesn't make any sense, feel free to send an email to
`jeff.elkner@gmail.com <mailto:jeff.elkner@gmail.com>`__ .


Coordinates
-----------

(0, 0) is at the top left of the window, with y increasing as you go downwards
and x increasing as you go rightwards.
The window is 640 pixels by 480, by default.
(You can make it a different size if you want to.)
Coordinates are given in units of one pixel.

All functions and objects that take coordinates take them as two separate parameters.

.. sourcecode:: python

    games.Circle(screen, 300, 200, ...)     # :) This is good
    games.Circle(screen, (300, 200), ...)   # :( This is bad

Colors
------

Before using the GASP color module with GASP games, you have to import it.

.. sourcecode:: python

    from gasp import color

To access the color module GASP has to offer, call ``color.*`` where ``*`` is
the color you wish to call. For example: ``color.BLACK``. This is the color
black. You can also access colors as a string using the ``color.from_str(string)`` method.

*Note:* color names must be in ALL CAPS.

Check out the gasp color reference chart to see all of the available color
options, or call ``color.available()`` to see the list of available colors.
At their core, colors in GASP games are just tuples of three numbers from 0 to 255, specifying
how much red, green, and blue (RGB) there is respectively in the color.


The Essentials
--------------

.. sourcecode:: python
    
    from gasp import games

    screen = games.Screen()
    screen.mainloop()

These are the essentials. ``from gasp import games`` imports the games module.
To access any functions or classes in the games module, call ``games.*`` where
``*`` is the name of the thing you want to access.

Most of these classes are designed to be extended by subclassing. To avoid some of the syntactic
pain that usually attends this kind of design in Python, every class defines an extra method, which
takes exactly the same arguments as its ``__init__`` method. In a class called ``Foobar``, this method is
called ``init_foobar``.

The games module is used by setting up objects which do what you want, and then calling the ``mainloop`` method of the
``Screen`` class. The ``mainloop`` method handles moving your objects around and updating the screen. ``mainloop`` does not
return until the player quits the game, so all the behavior of the game must go inside the objects you create before calling
``mainloop``.

``screen = games.Screen()`` creates your screen and prepares it to be run.

``screen.mainloop()`` runs your screen.

It's dead simple, but also dead necessary.


Graphics Objects
------------------


Screen
~~~~~~~~~~~~~~~~

The ``Screen`` class provides a region in which objects can exist and move.

The methods provided by the ``Screen`` class are listed below. We've listed them as you
would call them. If you make a subclass of the screen class and want to redefine these
methods, you'll need to add a ``self`` as the first parameter of the method.

- ``init_screen(width=640, height=480, title="GASP Games")``
    This method creates the window on the screen with the specified width, height.
    You can change the title of the screen using the ``title`` parameter.
- ``is_pressed(key)``
    This method returns 1 if the key is pressed, and 0 if it is not.
    The key names are in the games module. The letter keys are ``games.K_a`` to
    ``games.K_z``. The space bar is ``games.K_SPACE`` and the enter key is
    ``games.K_RETURN``. For a full list of keys, either check ``dir(games)`` or
    read the official Pygame documentation for keys `here <https://www.pygame.org/docs/ref/key.html>`__.

    *Note:* Most keyboards can only tell you about 4 keys being pressed at a time.
    So once you're pressing more than 4 keys, ``is_pressed`` might return 0 even for keys
    which are pressed.
- ``keypress(key)``
    This method is another way of getting key presses. It is called automatically
    whenever a key is pressed. In the standard ``Screen`` class, it does nothing.
    If you override it in a subclass of ``Screen``, you can get your subclass to handle
    the key presses. The ``key`` parameter will be the key which has just been pressed,
    using the same key names as the ``is_pressed`` function above.

    Whether you want to handle keys by writing your own ``keypress`` method or using the
    ``is_pressed`` method depends on what you're doing: in all the example sheets, other
    objects call the ``Screen``'s ``is_pressed`` method to find out whether a particular
    key is pressed, rather than the ``Screen`` handling the key press itself.
- ``mouse_buttons()``
    In exactly the same way as ``is_pressed``, this method returns the pressed-ness of the
    mouse buttons. It returns a *tuple* of three numbers, being the state of the left, middle,
    and right buttons respectively. The state is set to 1 if the button is pressed, and to 0 if
    it isn't.

    *Note:* Most Windows computers don't have a middle mouse button, so you might not want to
    use it for anything important.
- ``mouse_position()``
    This method returns the current position of the pointer in the window as an (x, y) pair.
    You usually need this information if you're handling a mouse click!
- ``mouse_down((x, y), button)``
    As with key presses, you can wait to be told about mouse clicks rather than actively
    scanning for them. This method is called automatically whenever a mouse button is pressed.
    In the standard ``Screen`` class, it does nothing. If you override it in a subclass of
    ``Screen``, you can get your subclass to handle mouse clicks: the ``x`` and ``y`` get the
    position of the pointer on the screen, while ``button`` is the number of the mouse button
    being pressed: 0 for left, 1 for middle, and 2 for right.

    Whether you want to handle mouse clicks by writing your own ``mouse_down`` routine method
    or by using the ``mouse_buttons`` routine instead depends on what you're doing.
- ``mouse_up((x, y), button)``
    This method is exactly like ``mouse_down`` except that it’s called when the mouse button is
    released. For a lot of purposes you will want to use this rather than ``mouse_down``,
    unless speed is more important than accuracy!
- ``set_background(background)``
    This method sets the background of the screen. You need to give it an image which you’ve loaded
    with ``load_image``, not the filename of the image. Note that the background image should not
    have transparency set, or you’ll get weird effects.
- ``set_background_color(color)``
    Sets the background color of the screen. ``color`` is a tuple of three numbers between 0 and 255
    indicating how much red, green and blue (RGB) there is respectively in the color.
    The ``color`` module provides useful constants for common colors. (See the ``Colors`` section above)
- ``set_title(title)``
    Sets the title of the ``Screen`` to ``title``. ``title`` is a string.
- ``tick()``
    This method is called every timer tick (usually 1/50th of a second). In the standard ``Screen`` class,
    it does nothing. If you make a subclass of the ``Screen`` class, you can override this method to do
    whatever you want to do every tick.

    *Note:* You can change the tick rate with the optional ``fps`` parameter when calling ``mainloop()``.
- ``handle_events()``
    This method enables you to write your own handler for the Pygame events, rather than using the standard
    one which comes with the ``Screen`` class. It is called every timer tick after all the objects have been
    updated. You probably don’t want to mess with this function unless you understand Pygame’s events module.

    The standard handler calls ``keypress`` to deal with key down events, ``mouse_down`` to deal with mouse
    down events, ``mouse_up`` to deal with mouse up events, and ``quit`` to deal with quit events.

    If you override this method, you **must** handle the quit event by calling ``self.quit()``. If this is confusing
    to you, try not to override ``handle_events``.
- ``quit()``
    Calling this method will stop the main loop from running and make the graphics window disappear.
- ``clear()``
    Calling this method will destroy all the ``Object``'s on the screen.
- ``mainloop(fps=50)``
    This method should be called once you’ve set up all the objects on the screen. This method won’t return until the
    screen’s ``quit`` method is called. It contains the loop which causes the objects on the screen to be drawn and
    redrawn, so nothing will move on the screen before you call it.
    The ``fps`` parameter is the number of times the screen should be updated every second. Whether or not your
    computer will actually achieve this depends on how fast a computer you have.
- ``overlapping_objects(box)``
    Returns a list of all the ``Object``'s on the Screen whose bounding boxes overlap the box you give. A “bounding box”
    is a rectangle, with the edges either horizontal or vertical, which completely encloses shape you see on the screen.

    The box should be given as a sequence of four elements, which are the co-ordinates of the top left of the box,
    and its width and height, like this: ``(top, left, width, height)``.
- ``all_objects()``
    Returns a list of all ``Object``'s on the screen.
- ``get_width()``
    Returns the width of the screen.
- ``get_height()``
    Returns the height of the screen.
- ``center_pos()``
    Returns a tuple of the coordinates of the center point on the screen.


Object
~~~~~~~~~~~~

The ``Object`` class represents a graphical object on the screen. You shouldn't create ``Object``'s directly:
the GASP games module provides subclasses of the ``Object`` class for you to use.
Here is a list of all the methods that the subclasses of ``Object`` have in common:

- ``destroy()``
    Removes the object from the ``Screen``.
- ``pos()``
    Returns position of the object’s *reference point*. When we say reference point, we mean some special point
    of the object: for a circle, this is the center. The description of each subclass of object will tell you
    what its reference point is.

    The point is returned as a tuple: so you can say ``(x, y) = my_object.pos()``.
- ``xpos()``
    Returns the x-coordinate of the object's reference point.
- ``ypos()``
    Returns the y-coordinate of the object's reference point.
- ``bbox()``
    Returns a bounding box for the object. A “bounding box” is a rectangle, with the edges either horizontal
    or vertical, which completely encloses shape you see on the screen. The box is returned as a tuple of 4 numbers,
    which are the coordinates of the top left of the box, and its width and height, like this:
    ``(top, left, width, height)``.
- ``move_to(x, y)`` *or* ``move_to((x, y))``
    Moves the object's reference point to ``(x, y)``, moving the object with it.
- ``move_by(dx, dy)`` *or* ``move_by((dx, dy))``
    Moves the object's reference point by ``dx`` in the x direction and ``dy`` in the y direction,
    moving the object with it.
- ``rotate_to(angle)``
    Sets the angle by which the object is rotated, in anticlockwise degrees (just like from math class).
    An angle of 0 means the angle at which the object was originally placed.

    For some objects (circles, for example), this may actually do nothing, except that the angle is remembered
    for returning from ``angle()``.
- ``rotate_by(angle)``
    Adjusts the angle by which the object is rotated, increasing it by ``angle`` degrees (anticlockwise).

    For some objects (circles, for example), this may actually do nothing, except that the angle is remembered
    for returning from ``angle()``.
- ``angle()``
    Returns the object's current angle, in anticlockwise degrees. The angle is always in the range of (0, 360).
- ``overlaps(object)``
    Returns True or False depending on whether or no if this object overlaps with the passed object.
- ``overlapping_objects()``
    Returns a list of the objects which are overlapping this object.
- ``filter_overlaps(object)``
    This is a utility method which allows for pixel perfect collision.

    Pixel perfect collision means that a "collision" between two objects will not be registered at ``True`` unless
    the actual pixels of both objects intersect. This eliminates the weird situation where two objects do not appear
    to be touching, but the library thinks they are.

    You will not need to use this method unless you create your own subclasses of ``Object``.
- ``collidepoint((x, y))``
    Returns ``True`` if the given point overlaps the ``Object``. Use this in conjunction with ``mouse_position()`` to
    test for clicking on an object.
- ``raise_object(above=None)``
    One way of viewing objects on a screen is with the objects being in different layers, the front of the screen
    being the “topmost” layer. Objects in “higher” layers then appear in front of (or “above”) other objects.

    This method brings the object towards the front of the screen, placing it “above” the other object above.
    If ``above`` is ``None`` or missing, then the object is pulled to the topmost layer.
- ``lower_object(below=None)``
    The reverse of ``raise_object``, this puts the object behind ``below``. If ``below`` is missing or None,
    the object is put to the “bottom” of the screen, behind everything except the background.


Dynamic Object Colors
~~~~~~~~~~~~~~~~~~~~~~~~~

``Polygon``, ``Ellipse``, ``Circle``, ``Arc``, ``Line``, and ``Text`` objects all have dynamic color functionality.
``Polygon``, ``Ellipse``, and ``Circle`` have all the following methods,
but ``Arc``, ``Line``, and ``Text`` do *not* have outline functionality.
Methods provided by the ``ColorMixin`` class and ``OutlineMixin`` class.
*Note:* see the ``Colors`` section above for more information about colors in GASP games.

- ``set_color(color)``
    This sets the main “fill” color of the object. ``color`` is a tuple of three numbers from 0 to 255, specifying
    how much red, green and blue there is respectively in the color. The ``color`` module provides constants for
    many colors to avoid you having to experiment.
- ``get_color()``
    This method returns the "fill" color of the object.
- ``set_outline(color)``
    This sets the color of the outline of an object. ``color`` is a tuple of three red, green, blue values, as
    explained above.
- ``get_outline()``
    This method returns the current outline color of the object.


Sprite
~~~~~~~~~~~~~

This class represents an image you've loaded in from a file or a surface you created (only if you understand Pygame).
``Sprite`` is a subclass of ``Object``, so it has all the methods from the ``Object`` class as well as this:

- ``init_sprite(screen, x, y, image, angle=0, static=0)``
    ``screen`` is the ``Screen`` which the image will be on. The center of the image will be at ``(x, y)``, which is
    also the reference point.

    ``image`` should be an image returned from the ``load_image`` function. See below for more details on that function.

    ``angle`` is the angle of rotation you want the image to start at.

    ``static`` is a flag that can be set to 1 if the sprite isn’t intended to move ever. When there are a lot of static
    objects and not too much else this can improve redrawing speed, but if there are only one or two it can actually
    slow things down.


Line
~~~~~~

This class represents a line on a ``Screen``. It starts at one point, and ends at another.

``Line`` is a subclass of ``Object``, so it inherits all the methods from ``Object``. It has these methods of its own:

- ``init_line(screen, x, y, points, color, static=0, thickness=1)``
    ``screen`` is the ``Screen`` that the ``Line`` is on.

    ``x`` and ``y`` are the coordinates of the reference point of the ``Line``.

    ``points`` is a list of pairs of coordinates ``[(x0, y0), (x1, y1)]``, giving the x and y coordinates of
    the points of the ``Line``. The coordinates are written *relative to the reference point*, that is, when you’re writing
    them, assume the reference point is at (0,0).

    ``color`` is the color of the line. This is a tuple of three numbers from 0 to 255, specifying how much red, green
    and blue respectively there is in the color. The ``color`` module provides a number of useful predefined colors.

    ``static`` is a flag that can be set to 1 if the line isn’t intended to move ever. When there are a lot of static
    objects and not too much else this can improve redrawing speed, but if there are only one or two it can actually
    slow things down.

    ``thickness`` is the thickness of the line. If this is set too high, the line might not look right. Mess around and
    find a good thickness value for what you want.
- ``set_points(points)``
    Change the points at the end of the line. The new ``points`` is specified in exactly the same way as the ``points``
    argument to ``init_line``. The position of the reference point on the screen stays the same.
- ``get_points()``
    Returns the current points as a list of pairs of coordinates.


Polygon
~~~~~~~~~~~~~~

This class represents a closed polygon on a ``Screen``. When we say *closed*, we mean that the lines making up the polygon
completely enclose a single area. A square is an example of a closed polygon. A square with one side removed is not closed.

``Polygon`` is a subclass of ``Object``, so it inherits all the methods from ``Object``. It has these methods of its own:

- ``init_polygon(screen, x, y, shape, color, filled=True, outline=None, static=0, thickness=1)``
    ``screen`` is the ``Screen`` that the ``Polygon`` is on.

    ``x`` and ``y`` are the coordinates of the reference point of the ``Polygon``.

    ``shape`` is a list of pairs of coordinates ``[(x0, y0), (x1, y1), ...]``, giving the x and y coordinates of
    the corners of the shape. The coordinates are written *relative to the reference point*, that is, when you’re writing
    them, assume the reference point is at (0,0).

    ``color`` is the color of the polygon. This is a tuple of three numbers from 0 to 255, specifying how much red, green
    and blue respectively there is in the color. The ``color`` module provides a number of useful predefined colors.

    If ``filled`` is True, the polygon is filled in with its ``color``.
    If ``filled`` is False, only the outline of the polygon is drawn.

    ``outline`` is the color of the outline of the polygon (if it needs to be different from the main color).

    ``static`` is a flag that can be set to 1 if the polygon isn’t intended to move ever. It is mostly used for the
    ``GridBox`` class in the ``boards`` module, which form the squares of a board. When there are a lot of static
    objects and not too much else this can improve redrawing speed, but if there are only one or two it can actually
    slow things down.

    ``thickness`` is the thickness of the outline of the polygon.
    If this is set too high, the outline might not look right.
    Mess around and find a good thickness value for what you want.
- ``set_shape(shape)``
    Change the shape of the polygon. The new ``shape`` is specified in exactly the same way as the ``shape`` argument to
    ``init_polygon``. The position of the reference point on the screen stays the same.
- ``get_shape()``
    Return the current shape as a list of pairs of coordinates.


Ellipse
~~~~~~~~~~

This class represents an ellipse on a ``Screen``. It is a subclass of ``Object``, so it inherits all the methods
from ``Object``. It has these methods of its own:

- ``init_ellipse(screen, x, y, x_radius, y_radius, color, filled=True, outline=None, static=0, thickness=1)``
    ``screen`` is the ``Screen`` that the ``Ellipse`` is on.

    The center of the ellipse is at ``(x, y)``. This is also the reference point.

    ``x_radius, y_radius`` are the radii in the ``x`` and ``y`` directions respectively.

    ``color`` is the color of the ellipse.

    If ``filled`` is True, the ellipse is filled.
    If ``filled`` is False, only the outline of the ellipse is drawn.

    ``outline`` is the color of the edge of the ellipse (if it needs to be different from the main color).

    ``static`` is a flag that can be set to 1 if the ellipse isn’t intended to move ever. When there are a lot of
    static objects and not too much else this can improve redrawing speed, but if there are only one or two it can
    actually slow things down.

    ``thickness`` is the thickness of the outline of the ellipse.
    If this is set too high, the outline might not look right.
    Mess around and find a good thickness value for what you want.
- ``set_radius(x_radius, y_radius)``
    Sets the two radii of the ellipse to ``x_radius`` and ``y_radius``. Passing a tuple is acceptable, but two values
    must be passed to avoid raising an error. If you are looking for a uniform radius, use the ``Circle`` class instead.
- ``get_radius()``
    Returns ``x_radius`` and ``y_radius`` as a tuple.



Circle
~~~~~~~~~~~~

This class represents a circle on a ``Screen``. It is a subclass of ``Ellipse``, so it inherits all the methods from
``Ellipse``. It has these methods of its own:

- ``init_circle(screen, x, y, radius, color, filled=True, outline=None, static=0, thickness=1)``
    ``screen`` is the ``Screen`` that the ``Circle`` is on.

    The center of the circle is at ``(x, y)``. This is also the reference point.

    ``radius`` is the radius of the circle.

    ``color`` is the color of the circle.

    If ``filled`` is True, the circle is filled.
    If ``filled`` is False, only the outline of the circle is drawn.

    ``outline`` is the color of the edge of the circle (if it needs to be different from the main color).

    ``static`` is a flag that can be set to 1 if the circle isn’t intended to move ever. When there are a lot of
    static objects and not too much else this can improve redrawing speed, but if there are only one or two it can
    actually slow things down.

    ``thickness`` is the thickness of the outline of the circle.
    If this is set too high, the outline might not look right.
    Mess around and find a good thickness value for what you want.
- ``set_radius(radius)``
    Set's the circle's radius to ``radius``.
- ``get_radius()``
    Returns the circle's current radius.


Arc
~~~~~

This class represents an arc drawn on a ``Screen``. It is a subclass of ``Object``, so it inherits all the methods from
``Object``. Notice that the reference point is the center of its elliptical orbit, which may not be what you would
expect. All angles are measured in degrees from the +x axis.

*Note:* Rotating an ``Arc`` will move it along its elliptical orbit. This may not be what you would expect.

It has these methods of its own:

- ``init_arc(screen, x, y, radius, color, start_angle, end_angle, static=0, thickness=1)``
    ``screen`` is the ``Screen`` that the ``Arc`` is on.

    ``(x, y)`` is the center of the arc's elliptical orbit. This is also the reference point.

    ``radius`` can be either a single value for a circular orbit, or a tuple of values ``(x_radius, y_radius)`` for an
    elliptical orbit.

    ``color`` is the color of the arc.

    ``start_angle`` and ``end_angle`` are the angles to draw the arc. Angles are measured in degrees from the +x axis.

    ``static`` is a flag that can be set to 1 if the arc isn’t intended to move ever. When there are a lot of
    static objects and not too much else this can improve redrawing speed, but if there are only one or two it can
    actually slow things down.

    ``thickness`` is the thickness of the arc.
    If this is set too high, the outline might not look right.
    Mess around and find a good thickness value for what you want.
- ``set_angles(start_angle, end_angle)``
    Sets the angles of the arc to run from ``start_angle`` to ``end_angle``. Angles are measured in degrees from the
    +x axis.
- ``get_angles()``
    Returns the start and end angles of the arc as a tuple.
- ``set_radius(x_radius, y_radius=None)``
    Sets the radii of the elliptical orbit to ``x_radius`` and ``y_radius``. If you pass only one value, the result
    will be a circular orbit. The reference point does not change.
- ``get_radius()``
    Returns ``x_radius`` and ``y_radius`` as a tuple. If the orbit is circular, only the ``radius`` is returned.
- ``rotate_by(angle)``
    Rotates the arc by ``angle`` degrees *along its elliptical orbit*. This is important to mention because it may not
    be what you expect.
- ``rotate_to(angle)``
    Rotates the arc to "face" in the direction of ``angle`` degrees *along its elliptical orbit*. The center of the
    arc's extent will be facing in the direction of ``angle``.


Text
~~~~~~~~~~

This class represents some text on a ``Screen``. It is a subclass of ``Object``, so it inherits all the methods from ``Object``.
Notice that the reference point is the center of the text, which is not usually what you expect. ``Text`` has these methods
of its own:

- ``init_text(screen, x, y, text, size, color, font=None static=0)``
    ``screen`` is the ``Screen`` that the ``Text`` is on.

    ``x`` and ``y`` are the coordinates of the center of the text. The reference point is ``(x, y)``.

    ``text`` is a string containing the text to be placed on the screen.

    ``size`` is the height of the text.

    ``color`` is the color of the text.

    ``font`` is a valid font string, or ``None``. If font is ``None``, a generic font is used. A valid font string is
    either a path to a .ttf font file or a valid system font.
    Check the list of valid system fonts with ``available_fonts()``.

    *Note:* If ``font`` is an invalid system font, the library will default to a generic font and alert the user.
    This will save the day if someone uses a system font on their machine, but it is not available when running on
    another machine.

    ``static`` is a flag that can be set to 1 if the text isn’t intended to move ever. When there are a lot of static
    objects and not too much else this can improve redrawing speed, but if there are only one or two it can actually
    slow things down.
- ``set_text(text)``
    Sets the current text to ``text``.
- ``get_text()``
    Returns the current text as a string.
- ``set_font(font)``
    Sets the font to ``font``. ``font`` is a valid font string, or ``None``.
- ``available_fonts()``
    Returns a list of available system fonts. Any string in this list counts as a valid font string.

    *Note:* This method is a *staticmethod* meaning you can even access it from the ``Text`` class itself.
    e.g. ``games.Text.available_fonts()``


Timer
~~~~~~~~~~~

The ``Timer`` class is a class you can add to something which is also a subclass of ``Object``, to make an object that
performs actions at regular intervals. A class which is intended to be used with another class is called a “mix-in”.
For instance, if you wanted to make a new class of your own which was a ``Circle`` and also a ``Timer``, you would
define the class by saying ``class MyClass(games.Circle, games.Timer):``. ``Timer`` has these methods of its own:

- ``init_timer(interval, running=1)``
    ``interval`` is how often the ``tick`` method is called, measured in timer ticks. How long a tick is depends on the
    ``fps`` argument you give to the ``Screen``'s ``mainloop`` method. Setting ``fps`` to 50 means a tick is 1/50 of a
    second.

    ``running`` controls whether the timer starts immediately on initialisation, or whether you need to explicitly
    ``start`` it later. You must call this method *after* you have called the ``init_`` method for the ``Object`` you
    are using.
- ``stop()``
    Stop the timer running. It continues to exist, but doesn't count anymore.
- ``start()``
    Starts the timer again. A full ``interval`` will elapse before it ticks.
- ``get_interval()``
    Gets the current interval.
- ``set_interval(interval)``
    Sets the current interval to ``interval``.
- ``tick()``
    This method *must be supplied* by you by subclassing the ``Timer`` class. The ``tick`` method gets called every
    tick. By default, it does nothing, but you should override it to do whatever you want.


Mover
~~~~~~~~~~~

The ``Mover`` class is a subclass of the ``Timer`` class. A ``Mover`` is something which moves itself around the screen
at regular intervals.

Like the ``Timer`` class itself, the ``Mover`` class is intended to be used as a mix-in class with a subclass of ``Object``.
For example, to create your own class representing a moving polygon, you would say
``class Ship(games.Polygon, games.Mover):``. ``Mover`` has these methods of its own.

- ``init_mover(dx, dy, da=0)``
    You must call this method *after* you have called the ``init_`` method for the ``Object`` subclass you are using.

    Every tick (see ``Timer``, above), your object will ``move_by`` ``dx`` pixels in the x direction and ``dy`` pixels
    in the y direction. If ``da`` is given and is non-zero the object will also be rotated by ``da`` degrees.
    The object’s ``moved`` method will then be called.
- ``set_velocity(dx, dy)`` or ``set_velocity((dx, dy))``
    Sets the velocity to ``dx`` in the x direction and ``dy`` in the y direction.
- ``get_velocity()``
    Returns the current velocity ``(dx, dy)`` as a tuple.
- ``set_angular_speed(da)``
    Sets the angular speed to ``da`` in degrees.
- ``get_angular_speed()``
    Returns the current angular speed.
- ``moved()``
    You must supply the ``moved`` method by subclassing. By default, the ``moved`` method does nothing, but you should
    override it to do whatever you want. The ``moved`` method is called on each tick after the object has been moved
    by ``(dx,dy)`` and rotated by ``da``.

    *Note:* if all of dx, dy, and da are zero, this method is still called each tick.


Message
~~~~~~~~~~~

The ``Message`` class is a subclass of the ``Text`` class and the ``Timer`` class. A ``Message`` is a ``Text`` object
that will go away after a specified period.

- ``init_message(screen, x, y, text, size, color, lifetime, after_death=None)``
    Except for the last two arguments, this method works identically to the ``init_text`` method (see the Text class we
    mentioned previously).

    ``lifetime`` is the lifetime of the message in ticks, that is, how long it will last before it goes away.

    When the message goes away, the ``after_death`` argument will be called if it was supplied. If it is supplied, the
    ``after_death`` argument must be something which is callable (a function, for example).


Animation
~~~~~~~~~~~~~

The ``Animation`` class is a subclass of the ``Sprite`` and ``Timer`` classes. It allows you to produce animations,
that is, pictures on the screen which change every tick.

``Animation`` has these methods of its own:

- ``init_animation(screen, x, y, nonrepeating_images, repeating_images, n_repeats=0, repeat_interval=1, angle=0)``
    ``screen`` is the ``Screen`` the ``Animation`` is on.

    ``(x, y)`` are the coordinates of the center of the animation.

    ``nonrepeating_images`` is a list of images from ``load_image`` or a list of filenames. You can automatically
    generate an animation list using ``load_animation``.

    ``repeating_images`` is a list of images from ``load_image`` or a list of filenames. You can automatically
    generate an animation list using ``load_animation``.

    ``n_repeats`` is the number of times the images in the ``repeating_images`` list will be shown. Set it to ``-1`` to keep
    repeating forever.

    ``repeat_interval`` is the number of ticks (see the ``Timer`` class) between one image and the next.

    ``angle`` is the starting angle of the ``Animation``.

    *Note:* If the ``nonrepeating_images`` list is ``[a, b, c, d]`` and the ``repeating_images`` list is ``[x, y, z]``
    then the sequence of images shown will be ``a, b, c, d, x, y, z, x, y, z, x, y, z, ....``
- ``play_animation(animation, n_repeats=1)``
    Plays an animation a specified number of times.

    ``animation`` is a list of images from ``load_image`` or a list of filenames. You can automatically
    generate an animation list using ``load_animation``. *Note:* If passing a list of filename strings, making the list
    too long may slow down the game. In that case, try loading the animation first using ``load_animation`` then
    passing it to ``play_animation``.

    ``n_repeats`` is the number of times to play the animation. If ``n_repeats`` is less than 1, an error will be
    raised.
- ``image_changed()``
    This method get automatically called whenever the ``Animation``'s image gets changed. Override this method to do
    whatever you want.


Utility Functions
~~~~~~~~~~~~~~~~~~~~~~~

- ``load_image(file, transparent=1)``
    Loads an image from a file and returns an object which can be passed to the ``games`` modules’ classes as the image
    for a ``Sprite`` or ``Background``.

    ``file`` is a string containing the name of the file to load. Be sure to include the file extension.

    ``transparent`` indicates whether the image should be transparent. If it is not zero, the image is transparent. The
    transparent color is taken from the top left corner of the image. You should not make transparent images which you
    intend to use as the background for the screen.

    *Note:* The object returned by ``load_image`` is a ``pygame.Surface`` object.
- ``load_sound(file)``
    Loads a sound object from a WAV file and returns an object which you can use to play the sound.
    When you want to play the sound, use its ``play()`` method.

    *Note:* The object returned by ``load_sound`` is a ``pygame.mixer.Sound`` object. For more information on the
    methods of sound objects see the official Pygame documentation for Sound
    `here <https://www.pygame.org/docs/ref/mixer.html#pygame.mixer.Sound>`_
- ``load_animation(files, transparent=1)``
    Loads a sequence of images from files like ``load_image``. Returns a list of images that can be used to
    construct an ``Animation``.

    ``files`` is a list of filename strings.
- ``scale_image(image, x_scale, y_scale=None)``
    This function scales an image from ``load_image`` by the amount you specify.

    If you give it only one scale factor, the image is scaled uniformly in both the x and y directions.
    (Giving a factor of 2 would double the size of the image, for example).

    If you give it two scale factors, you can scale the image by different amounts in the x and y directions
    respectively.

    This function returns the scaled image object. It does not modify the original image you gave it.
- ``set_resolution(image, x_resolution, y_resolution)``
    This function sets the resolution of the ``image`` to ``x_resolution`` by ``y_resolution``. This is similar to
    ``scale_image`` but instead of scaling, it simply sets the desired resolution.

    ``set_resolution`` returns an image object. It does not modify the original image you gave it.
- ``flip_image(image, vertical=False, horizontal=False)``
    This function flips an image from ``load_image`` by the parameters you specify. ``vertical`` flips vertically, and
    ``horizontal`` flips horizontally.

    ``flip_image`` returns a flipped image object. It does not modify the original image you gave it.
