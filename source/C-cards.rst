Graphics API for Students of Python: GASP - Cards Module
=========================================================

Introduction
-------------

This sheet is an extension of the GASP games module. See `Sheet H <H-games.html>`__ for more information on the games
module.

The cards module allows you to make games in Python using a standard deck of playing cards. A "standard" deck of
playing cards is one with 52 cards:

4 suits (Spades, Hearts, Diamonds, and Clubs), and each suit has 13 values (Ace, 2-10, Jack, Queen, King).

Examples of card games include solitaire and blackjack. All of these games use a standard deck of cards. This, of course,
is complicated to create, so the cards module is here to take the reigns.

This sheet is more of a reference than an introduction. If you get confused,
ask a teacher for help. If you *are* a teacher or you are studying this on your own
and it still doesn't make any sense, feel free to send an email to
`jeff.elkner@gmail.com <mailto:jeff.elkner@gmail.com>`__ .

*Note:* All the information in `Sheet H <H-games.html>`__ is still accurate when using the cards module.


The Essentials
---------------

.. sourcecode:: python

    from gasp import games
    from gasp import cards

    # Make the card deck the main focus of the screen with built-in card handling
    deck = cards.SingleDeck(...)
    deck.mainloop()

    # OR

    # Make the card deck just a part of a broader screen (does NOT have built-in card-handling)
    screen = games.Screen()
    deck = cards.Deck(screen, ...)
    screen.mainloop()

These are the essentials. ``from gasp import cards`` imports the cards module.
To access any functions or classes in the cards module, call ``cards.*`` where
``*`` is the name of the thing you want to access.

*Note:* You usually want to access the ``games`` module as well when working with ``cards``.
See `Sheet H <H-games.html>`__ for information on how to use the games module.

Most of these classes are designed to be extended by subclassing. To avoid some of the syntactic
pain that usually attends this kind of design in Python, every class defines an extra method, which
takes exactly the same arguments as its ``__init__`` method. In a class called ``Foobar``, this method is
called ``init_foobar``.

The cards module is used in one of two ways:

1. Making the deck the main focus of the screen (usually the easiest)
2. Making the deck just a part of a broader screen (usually harder)

To make the deck the main focus of the screen, use the ``SingleDeck`` class (or a subclass you create).
By default, the ``SingleDeck`` has built-in card handling. This means right out of the gate you can move the cards
around using the mouse. See the ``SingleDeck`` section below for more details.

To make the deck just a part of a broader screen, use the ``Deck`` class (or a subclass you create).
The ``Deck`` class does NOT have built-in card handling (since that would be the job of your ``Screen`` class).
If you subclass ``Deck`` you must handle the movement of the cards yourself. If that seems like too daunting of a task,
use ``SingleDeck`` instead.


Cards Objects
--------------


Card
~~~~~~

``Card`` is a subclass of ``Sprite`` from the ``games`` module. It is intended to be subclassed itself to fit the
exact needs of a particular game, since it provides only fairly general methods and properties. It is possible to create
standalone cards using the ``Card`` class, but it is recommended that you let your ``Deck`` class handle the creation
of ``Card``'s.

Primarily, the ``Card`` class provides a number of useful properties, variables that belong to each ``Card``
separately. These properties are:

- ``code``
    A string that encodes the card's value and suit.

    Values are encoded as either the number or capitalized first letter of their name:
        Ace: ``"A"``,
        2: ``"2"``,
        3: ``"3"``,
        4: ``"4"``,
        5: ``"5"``,
        6: ``"6"``,
        7: ``"7"``,
        8: ``"8"``,
        9: ``"9"``,
        10: ``"10"``,
        Jack: ``"J"``,
        Queen: ``"Q"``,
        King: ``"K"``

    Suits are encoded as the capitalized first letter of their name:
        Spades: ``"S"``,
        Hearts: ``"H"``,
        Diamonds: ``"D"``,
        Clubs: ``"C"``

    Card codes are always in the order: value, suit. For example, Ace of Spades is encoded as ``"AS"``, 3 of Hearts is
    encoded as ``"3H"``, and 7 of Clubs is encoded as ``"7C"``.
- ``value``
    A string that encodes only the value of the ``Card``. See above for more details.
- ``suit``
    A string that encodes only the suit of the ``Card``. See above for more details.
- ``color``
    The face color of the ``Card``.
- ``facing_up``
    Set to ``True`` if the card is currently facing up, otherwise ``False``.
    See below for more details about flipping cards over.
- ``deck``
    The ``Deck`` that this ``Card`` belongs to. If the ``Card`` is a standalone, ``deck`` is set to ``None``.

In addition to the methods inherited from ``Sprite``, the following methods can be used on a ``Card``:

- ``init_card(screen, x, y, code, size=70)``
    ``screen`` is the ``Screen`` that the card lives on.

    ``(x, y)`` is the screen coordinates of the center of the card.

    ``code`` is the encoded value and suit of the card (see above for more details).

    ``size`` is the height of the card in pixels.
- ``flip()``
    Flips the card over onto its backside.
    This conceals its face value to the player, but is still accessible by the game code.
- ``get_overlapping_cards(codes_only=False)``
    Returns a list of cards that are overlapping the selected card. If ``codes_only`` is ``True``, then the returned
    list will only contain the ``code`` strings of each of the overlapped cards.
- ``get_above()``
    Returns the card that is immediately above visually. If no card is found, returns ``None``.

    *Note:* This method only works for ``Card``'s that are in a ``Deck``.
- ``get_below()``
    Returns the card that is immediately below visually. If no card is found, returns ``None``.

    *Note:* This method only works for ``Card``'s that are in a ``Deck``.


Deck
~~~~~~

``Deck`` is a standard deck of 52 playing cards. Think about it as a container for ``Card`` objects.

The ``Deck`` class is where you keep the ``Card``'s in your deck. You will almost always want to subclass
``SingleDeck`` to automatically handle card movement. If you subclass ``Deck`` itself, you *must* handle card
movement in your ``Screen`` class manually.

Each ``Deck`` contains a "stack" of 52 ``Card``'s.

The ``Deck`` class provides useful properties (variables):

- ``stack``
    A list of all the ``Card``'s in this deck.
- ``start_x`` and ``start_y``
    The starting ``x`` and ``y`` screen coordinates of the deck.
- ``size``
    The assigned size of the ``Card``'s in this deck.

``Deck`` provides the following methods:

- ``init_deck(screen, x, y, size=70, facing_up=True)``
    ``screen`` is the ``Screen`` in which this deck lives.

    ``(x, y)`` is the starting position of the center of the deck.

    ``size`` is the height in pixels of the cards in this deck.

    ``facing_up`` determines whether or not the deck starts facing up or down.
- ``new_card(x, y, code)``
    You will probably want to override this method in any subclass that you create. This method is called automatically
    to create a new card at screen coordinates ``(x, y)`` . The version supplied with ``Deck`` itself creates a
    ``Card``, which is almost certainly not what you want. Instead you should write a version which returns one of the
    subclasses of ``Card`` that you have made.
- ``shuffle(lst=None)``
    This method shuffles a list of cards. By default, it shuffles the whole deck.
    It works best before any of the cards have been moved
    (or more specifically when all the cards are on top of each other).

    It is recommended that you call this somewhere inside your ``__init__`` routine or after moving all the cards in
    the deck to a single location.
- ``get_card(code)``
    Returns the ``Card`` object in the deck with the matching ``code``.

    If the ``code`` is invalid, an error will be raised.
- ``get_hovered((x, y))``
    Returns the card which is raised highest visually on the screen at ``(x, y)``. Will return ``None`` if no card is found.

    Use this in conjunction with ``mouse_position()`` (see `Sheet H <H-games.html>`__) to get the card you just clicked on.


SingleDeck
~~~~~~~~~~~~

The ``SingleDeck`` class is a subclass of ``Deck`` and ``Screen``. It automatically handles card movement using the mouse.
This means that it is the easiest way to plug-and-play with the ``cards`` module.
This is the class that you will probably want to subclass for your own game, unless for some reason you want to handle
card movement manually.

``SingleDeck`` provides two useful properties:

- ``grabbing``
    This is set to ``True`` when the player is currently moving around ("grabbing") a card.
- ``current_card``
    This returns the card that the player is currently holding, or the most recent card the player held.
    At the start of the game, ``current_card`` is set to ``None`` which may raise errors if you are not careful.

``SingleDeck`` provides these methods in addition to those found in ``Deck`` and ``Screen``:

- ``init_singledeck(x, y, size=70, facing_up=True, width=640, height=480, title="GASP Games")``
    ``(x, y)`` is the starting position of the center of the deck.

    ``size`` is the height in pixels of the cards in the deck.

    ``facing_up`` determines whether or not the deck starts facing up or down.

    ``width`` is the width of the screen in pixels.

    ``height`` is the height of the screen in pixels.

    ``title`` is the title of the window.

    Basically, enter the parameters for both ``init_deck()`` and ``init_screen()``.
- ``handle_cards(allow_flipping=False, auto_raise=True)``
    This is the method that automatically handles card movement for you. By default, this method is called every game
    tick. This means that if you overwrite the ``tick`` method of your screen, you want to call ``handle_cards()``
    somewhere inside your new tick method (preferably towards the beginning).

    ``allow_flipping`` determines whether or not to allow the player to flip over cards by right-clicking on them.
    This could be useful for games where the player is allowed to freely flip cards. By default, it is set to ``False``.

    ``auto_raise`` ensures that the current card is raised to the top of the screen. Set this to ``False`` if you
    want to handle exactly when cards get raised manually.
- ``on_card_grab()``
    This method is called automatically when grabbing a card. You should override it to do whatever you want.
    Use it when you only want something to run on the first frame of grabbing a card.

    *Note:* ``on_card_grab()`` is called *inside ``handle_cards``*. This means that if you don't call ``handle_cards``
    somewhere, this method will never run.


Pile
~~~~~~

``Pile`` is a subclass of ``Circle`` that provides a place to put cards neatly. The purpose of a ``Pile`` is to keep
all cards that are on top of it, in a single neat stack. Any cards that are within the pile will be pulled directly to
its center.

``Pile`` provides these methods of its own:

- ``init_pile(deck, x, y, color)``
    ``deck`` is the ``Deck`` that this ``Pile`` is responsible for.
    *Note:* If you have more than one ``Deck``, only the deck passed here will respond to the ``Pile``.

    ``(x, y)`` is the position of the center of the pile.

    ``color`` is the color of the pile.
- ``clean_pile()``
    This method neatly packs all cards within the pile's reach to its center. When all the cards are on top of each
    other like this, you can only grab the top card in the pile.

    If you want your subclass to organize its cards differently, you should override this method.

    It is recommended that you do some checks before calling this method every game tick. For example:

    .. sourcecode:: python

        # Inside of a SingleDeck init method
        self.pile = Pile(...)

        # Inside of a SingleDeck tick method
        if not self.grabbing:  # Don't move the card the player is currently holding
            self.pile.clean_pile()

- ``get_cards(codes_only=False)``
    Returns a list of all cards currently in the pile ordered from the bottom up. ``codes_only`` is the same as in
    ``get_overlapping_cards()``.


Utility Functions and Properties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``cards`` module also supplies some useful properties and functions. Remember, to reference anything in the ``cards``
module, it must be prefaced by ``cards.*`` where ``*`` is the name of the thing you want to access.

(See above for more details on what exactly a "code" is)

- ``VALUES``
    A list of all acceptable value codes in ascending order.
- ``SUITS``
    A list of all acceptable suit codes.
- ``CODES``
    A list of all acceptable card codes in ascending order.
- ``SPADES``
    A list of all acceptable codes in the spades suit in ascending order.
- ``HEARTS``
    A list of all acceptable codes in the hearts suit in ascending order.
- ``DIAMONDS``
    A list of all acceptable codes in the diamonds suit in ascending order.
- ``CLUBS``
    A list of all acceptable codes in the clubs suit in ascending order.

- ``get_suit_color(code)``
    Returns the color value of a given code. If the passed ``code`` is invalid, this method will return ``None``.
