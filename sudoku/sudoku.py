# GASP games Sudoku Sheet
# Written by Richard Martinez

from gasp import games
from gasp import boards
from gasp import color
from gasp.utils import random_between


# This import is only used to write the generate_sudoku_board function. Otherwise not needed here
from random import sample


# This function could be put inside gasp.utils?
# It may be a bit too complex to ask for the student to write themselves.
# It may even be worth considering placing other game-specific functions inside utils if
# they are too complex for the students to write themselves.
# If placed inside gasp.utils, it could be easily imported by using:
# from gasp.utils import generate_sudoku_board
def generate_sudoku_board():
    """
    Returns a 9 x 9 array of a valid filled sudoku board.

    Taken from:
    https://stackoverflow.com/questions/45471152/how-to-create-a-sudoku-puzzle-in-python
    """
    def pattern(r, c):
        return (3 * (r % 3) + r // 3 + c) % 9

    def shuffle(lst):
        return sample(lst, len(lst))

    base = range(3)
    rows = [g * 3 + r for g in shuffle(base) for r in shuffle(base)]
    cols = [g * 3 + c for g in shuffle(base) for c in shuffle(base)]
    nums = shuffle(range(1, 10))

    board = [[nums[pattern(r, c)] for c in cols] for r in rows]
    return board


BOX_SIZE = 50
MARGIN = BOX_SIZE + 10
BOARD_SIZE = 9

CANDIDATE_SIZE = 20
NUMBER_SIZE = 35

# Map Each Game Key, to it's corresponding integer
GAME_KEYS = {
    games.K_1: 1,
    games.K_2: 2,
    games.K_3: 3,
    games.K_4: 4,
    games.K_5: 5,
    games.K_6: 6,
    games.K_7: 7,
    games.K_8: 8,
    games.K_9: 9
}

# To place candidates in the nice order inside the cell, we make the CANDIDATE_OFFSETS
# dictionary. This contains tuples for how far to offset the candidate text with the GameCell.

# Positions relative to the cell
CANDIDATE_POSITIONS = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

# Create Offsets Dictionary
k = BOX_SIZE // 4  # Need this constant
CANDIDATE_OFFSETS = {}
for number in range(1, 10):  # 1 through 9
    for row in CANDIDATE_POSITIONS:
        if number in row:
            i = row.index(number)
            j = CANDIDATE_POSITIONS.index(row)

    CANDIDATE_OFFSETS[number] = ((i + 1) * k,
                                 (j + 1) * k)


class SudokuCell(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.correct_number = self.board.solution[j][i]  # j and i are REVERSED!
        self.image = None
        self.candidates = {}
        self.guessable = True

    def guess_number(self, number):
        if not self.guessable:  # Not allowed to change clues
            return

        previous_number = self.clear_cell()

        if previous_number != number:  # New number
            x, y = self.board.cell_to_coords(self.grid_x, self.grid_y)
            self.image = games.Text(self.screen,
                                    x + BOX_SIZE / 2,
                                    y + BOX_SIZE / 2,
                                    text=str(number),
                                    size=NUMBER_SIZE,
                                    color=color.BLACK)

    def guess_candidate(self, number):
        if self.get_number() or not self.guessable:  # If space is already filled
            return

        if number in self.candidates.keys():  # Number is already candidated
            self.candidates[number].destroy()  # Destroy it
            self.candidates.pop(number)
        else:  # New candidate
            x, y = self.board.cell_to_coords(self.grid_x, self.grid_y)
            x_offset, y_offset = CANDIDATE_OFFSETS[number]
            candidate = games.Text(self.screen,
                                   x + x_offset,
                                   y + y_offset,
                                   text=str(number),
                                   size=CANDIDATE_SIZE,
                                   color=color.BLACK)
            self.candidates[number] = candidate  # Make it

    def clear_cell(self):
        # Clear Candidates
        for number in self.candidates.keys():
            self.candidates[number].destroy()
        self.candidates = {}

        # Clear Image and Return Previous Number
        if self.image:
            previous_number = self.get_number()
            self.image.destroy()
            self.image = None
            return previous_number

    def get_number(self):
        if self.image:
            return int(self.image.get_text())


class Sudoku(boards.SingleBoard):
    def __init__(self, n_cols=9, n_rows=9, num_clues=25):
        self.solution = generate_sudoku_board()  # Returns a 9 x 9 array with the solution in it.
        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, BOX_SIZE, fill_color=color.WHITE)
        self.set_background_color(color.LIGHTGRAY)

        self.game_over = 0

        self.enable_cursor(0, 0)
        self.draw_all_outlines()

        self.draw_bold_lines()

        # Init Header Text
        x = self.get_width() / 2
        y = MARGIN / 2
        self.header = games.Text(self, x, y, "S  U  D  O  K  U", size=40, color=color.WHITE)

        # Add Clues
        clue_cells = 0
        while clue_cells < num_clues:  # Give the player 25 clues
            i = random_between(0, n_cols - 1)
            j = random_between(0, n_rows - 1)
            box = self.grid[i][j]
            if box.guessable:  # blank space
                box.guess_number(box.correct_number)
                box.guessable = False
                clue_cells += 1

        # Set all the Clues to BLUE text
        # Might be confusing. Just use a for loop?
        def apply(box):
            if box.image:  # At this point, only clue cells have their image shown
                box.image.set_color(color.BLUE)

        self.map_grid(apply)

    def new_gamecell(self, i, j):
        return SudokuCell(self, i, j)

    def tick(self):
        if self.game_over:
            return

        if self.detect_win():
            self.win_game()

    def detect_win(self):
        for row in self.grid:
            for box in row:
                if box.correct_number != box.get_number():  # If any of the cells are wrong, return False
                    return False
        return True  # Otherwise, we win!

    def win_game(self):
        self.game_over = 1
        self.disable_cursor()
        self.header.set_text("You Win!")
        self.header.set_color(color.GREEN)

    def keypress(self, key):
        if self.game_over:
            return

        self.handle_cursor(key)

        for game_key in GAME_KEYS.keys():  # Handle All the Number Keys
            if key == game_key:
                number = GAME_KEYS[game_key]
                if self.screen.is_pressed(games.K_LSHIFT):  # Hold Left Shift to Activate Candidate Mode
                    self.cursor.guess_candidate(number)
                else:
                    self.cursor.guess_number(number)

        # GIVE UP FOR TESTING PURPOSES (PROBABLY SHOULD BE REMOVED)
        if key == games.K_ESCAPE:
            self.game_over = 1

            def apply(box):
                if not box.image:
                    box.guess_number(box.correct_number)

            self.map_grid(apply)

            self.disable_cursor()
            self.header.set_text("You Gave Up!")
            self.header.set_color(color.RED)

    def draw_bold_lines(self):
        # Just for looks, helps to see which blocks are together
        vertical_line = [(0, 0), (0, BOARD_SIZE * BOX_SIZE)]
        horizontal_line = [(0, 0), (BOARD_SIZE * BOX_SIZE, 0)]

        cells = [(3, 0), (6, 0), (0, 3), (0, 6)]
        for x, y in cells:
            start_pos = self.cell_to_coords(x, y)
            if y == 0:
                line_type = vertical_line
            elif x == 0:
                line_type = horizontal_line

            # USES NEW GAMES LINE CLASS
            # NEED NEWEST VERSION OF GASP GAMES
            games.Line(self, start_pos[0], start_pos[1], line_type, color.BLACK, thickness=3)


# Easy: num_clues = 55
# Medium: num_clues = 45
# Hard: num_clues = 35
# Expert: num_clues = 25
sudoku = Sudoku(num_clues=45)  # Default to Medium Difficulty
sudoku.mainloop()
