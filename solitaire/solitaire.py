# GASP games Solitaire
# Written by Richard Martinez

from gasp import games
from gasp import cards
from gasp import color
from gasp.utils import distance

# The stacks needed to win the game
CORRECT_STACKS = {
    "S": cards.SPADES,
    "H": cards.HEARTS,
    "D": cards.DIAMONDS,
    "C": cards.CLUBS
}


class Foundation(cards.Pile):
    """
    A place to put cards in order.
    """
    def __init__(self, deck, x, y, suit):
        self.init_pile(deck, x, y, color.GRAY)
        self.suit = suit
        self.set_radius(30)

        games.Text(deck.screen, x, y, text=suit, size=30, color=color.WHITE)

    def check_pile(self):
        # Return True if pile is correct
        current_stack = self.get_cards(codes_only=True)
        correct_stack = CORRECT_STACKS[self.suit]
        return current_stack == correct_stack

    def clean_pile(self):
        # Only accept cards of the correct suit
        for code in CORRECT_STACKS[self.suit]:
            card = self.deck.get_card(code)

            if not card.facing_up:
                continue

            d = distance(self.pos(), card.pos())
            if d == 0:
                continue
            elif d < self.get_radius():
                card.move_to(self.pos())


class Base(cards.Pile):
    """
    A base for a Tableau.
    """
    def __init__(self, deck, index):
        self.index = index
        x, y = 80 * (index + 1), 130
        self.init_pile(deck, x, y, color.GRAY)
        self.set_radius(30)
        self.tableau = Tableau(deck)

    def clean_pile(self):
        # Update the stack
        self.tableau.generate_stack(self.get_lowest())
        for card in self.tableau.stack:
            card.move_to(self.pos())

        self.tableau.handle_tableau()
        x, y = self.pos()
        self.tableau.move_to(x, y)

    def get_lowest(self):
        # Get a sample card
        sample_card = None
        for object in self.overlapping_objects():
            if object in self.deck.stack:
                sample_card = object
                break

        return sample_card


class Tableau:
    """
    Handle card tower-like movement.
    """
    def __init__(self, deck):
        self.deck = deck
        self.stack = []  # List of held cards

    def handle_tableau(self):
        # Keep all the cards in a tower
        for card in self.stack:
            index = self.stack.index(card)
            if index < 1:  # Don't grab the first one
                continue

            prev_card = self.stack[index - 1]
            next_pos = prev_card.get_next_pos()
            card.move_to(next_pos)
            card.raise_object()

    def generate_stack(self, bottom):
        # Get a list of cards to be in the tower
        # From the bottom up
        self.stack = []

        if bottom is None:
            return

        self.stack.append(bottom)
        above = bottom.get_above()
        while above is not None:
            self.stack.append(above)
            above = above.get_above()

        for card in self.stack:
            card.raise_object()

    def move_to(self, x, y):
        for card in self.stack:
            index = self.stack.index(card)
            card.move_to(x, y + 20 * index)
            card.raise_object()


class SolitaireCard(cards.Card):
    def __init__(self, screen, x, y, code, size=70):
        self.init_card(screen, x, y, code, size)

    def get_next_pos(self):
        # Position of the next card in a tower
        x, y = self.pos()
        return x, y + 20


class Solitaire(cards.SingleDeck):
    def __init__(self):
        x, y = 50, 400
        self.init_singledeck(x, y, facing_up=False, title="Solitaire")
        self.set_background_color(color.GREEN)
        self.game_over = 0
        self.shuffle()

        # Create piles
        self.pile = cards.Pile(self, x, y, color.GRAY)
        self.junk = cards.Pile(self, self.get_width() - x, y, color.GRAY)

        # Create foundations
        self.foundations = [
            Foundation(self, 350, 50, "S"),
            Foundation(self, 425, 50, "H"),
            Foundation(self, 500, 50, "D"),
            Foundation(self, 575, 50, "C")
        ]

        # This is the main tower to be used by the mouse
        self.tableau = Tableau(self)

        # Create bases
        self.bases = []
        for i in range(7):  # Make 7 bases
            self.bases.append(Base(self, i))

        for base in self.bases:
            # Add the number of cards necessary
            for i in range(base.index + 1):
                # Grab a card from the deck
                card = self.get_hovered((self.start_x, self.start_y))
                card.move_to(base.pos())
            base.clean_pile()
            # Flip the last card in the stack
            base.tableau.stack[-1].flip()

    def new_card(self, x, y, code):
        return SolitaireCard(self.screen, x, y, code, self.size)

    def tick(self):
        if self.game_over:
            return

        # Flipping and raising are handled manually
        self.handle_cards(allow_flipping=False, auto_raise=False)

        if not self.grabbing:
            # Clean all the piles
            self.pile.clean_pile()
            self.junk.clean_pile()

            for foundation in self.foundations:
                foundation.clean_pile()

            for base in self.bases:
                base.clean_pile()

            # Snap cards to tableau
            if self.current_card:
                below = self.current_card.get_below()

                for base in self.bases:
                    if below in base.tableau.stack:
                        last_card = base.tableau.stack[-1]
                        for card in self.tableau.stack:
                            if card in base.tableau.stack:
                                continue

                            card.move_to(last_card.get_next_pos())
                        base.clean_pile()

            # Clear junk
            if len(self.pile.get_cards()) == 0:
                for card in self.junk.get_cards():
                    card.move_to(self.pile.pos())
                    card.flip()
                self.shuffle(lst=self.pile.get_cards())

            # Flip over free cards
            for card in self.stack:
                if card.pos() != self.pile.pos():
                    if not card.facing_up:
                        if card.get_above() is None:
                            card.flip()
        else:
            # Handle the main mouse tower
            self.tableau.handle_tableau()

        if self.detect_win():
            self.win_game()

    def on_card_grab(self):
        # Only generate the main tower when first grabbing a card
        self.tableau.generate_stack(self.current_card)

    def detect_win(self):
        # When all the foundations are correct, you win!
        for foundation in self.foundations:
            if not foundation.check_pile():
                return False
        return True

    def win_game(self):
        self.game_over = 1
        x, y = self.center_pos()
        games.Text(self, x, y, "You Win!", size=50, color=color.WHITE)


solitaire = Solitaire()
solitaire.mainloop()
