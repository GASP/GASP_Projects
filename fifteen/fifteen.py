# GASP games Fifteen Sheet
# Written by Silas Riggs

from gasp import games
from gasp import color
from random import randint
from math import floor  # This rounds down to the nearest integer

FRAGMENT_SIZE = 150
MARGIN = 60
SCREENWIDTH = 4 * FRAGMENT_SIZE + 2 * MARGIN
SCREENHEIGHT = SCREENWIDTH
SPEED = 50


def get_coords(i, j):
    return (MARGIN + i * FRAGMENT_SIZE + FRAGMENT_SIZE / 2,
            MARGIN + j * FRAGMENT_SIZE + FRAGMENT_SIZE / 2)


class Picture(games.Screen):
    def __init__(self):
        self.init_screen(SCREENWIDTH, SCREENHEIGHT)
        self.set_background_color(color.LIGHTGRAY)
        self.image = []

        # For each box that is not the bottom right box, load a partial picture of Jen in that box
        for i in range(4):
            self.image.append([])
            for j in range(4):
                if i != 3 or j != 3:
                    fragment = games.load_image("assets/jen" + str(i + 4 * j) + ".bmp", transparent=0)
                    (x, y) = get_coords(i, j)
                    self.image[i].append(Block(self, x, y, fragment))

                # In the bottom right box, set the third and final element to "None"
                else:
                    self.image[3].append(None)

                self.blank_i = 3
                self.blank_j = 3

        # This entire for loop shuffles the puzzle
        # Get the pixel each box is centered on (x, y)
        for i in range(4):
            for j in range(4):
                while 1:
                    # Find a random box to swap with
                    i1 = randint(0, 3)
                    j1 = randint(0, 3)

                    # If the swap changes parity, break and move on. If it doesnt change parity, look for another box that does
                    # (i, j) is the same box as (i1, j1), (i,j) is the blank box, (i1, j1) is the blank box
                    if (i == i1 and j == j1) or (i == self.blank_i and j == self.blank_j) or (
                            i1 == self.blank_i and j1 == self.blank_j):
                        break

                (x, y) = get_coords(i, j)
                (x1, y1) = get_coords(i1, j1)

                # In the array, switch the locations of the box at (i, j) and the box at (i1, j1)
                temp = self.image[i][j]
                self.image[i][j] = self.image[i1][j1]
                self.image[i1][j1] = temp

                # If the randomly picked box is not empty, then move it visually. If it is empty, set the blank variables to i and j
                if self.image[i][j] is not None:
                    self.image[i][j].slide_to(x, y)
                else:
                    self.blank_i = i
                    self.blank_j = j

                # If the non-random box is not empty, then move it visually. If it is empty, set the blank variables to i1 and j1
                if self.image[i1][j1] is not None:
                    self.image[i1][j1].slide_to(x1, y1)
                else:
                    self.blank_i = i1
                    self.blank_j = j1

    # When the mouse button is released, find the grid box that the cursor is in (i and j)
    def mouse_up(self, pos, button):
        i = (pos[0] - MARGIN) / FRAGMENT_SIZE
        j = (pos[1] - MARGIN) / FRAGMENT_SIZE
        i = floor(i)
        j = floor(j)

        # If the cursor is inside the margin when clicked
        if 0 <= i < 4 and 0 <= j < 4:
            # If the clicked box is next to the blank space
            if abs(i - self.blank_i) + abs(j - self.blank_j) == 1:
                # Find the (x, y) coordinates of the blank box and visually move the clicked box to them
                (x, y) = get_coords(self.blank_i, self.blank_j)
                self.image[i][j].slide_to(x, y)

                # Switch the blank box and clicked box in the array
                self.image[self.blank_i][self.blank_j] = self.image[i][j]
                self.image[i][j] = None
                self.blank_i = i
                self.blank_j = j

    # When the escape button is pressed quit the game
    def keypress(self, key):
        if key == games.K_ESCAPE:
            self.quit()


class Block(games.Sprite, games.Mover):
    def __init__(self, screen, x, y, image):
        self.init_sprite(screen, x, y, image)
        self.init_mover(0, 0)
        self.target_x = x
        self.target_y = y

    # Get the location of the current box, assign this location to (old_x, old_y)
    def slide_to(self, x, y):
        self.target_x = x
        self.target_y = y
        (old_x, old_y) = self.pos()

        # If the box needs to slide right, set dx to SPEED. If it needs to slide left, set dx to -SPEED
        if old_x < x:
            dx = SPEED
        elif old_x > x:
            dx = -SPEED
        else:
            dx = 0

        # If the box needs to slide up, set dy to SPEED. If it needs to slide down, set dy to -SPEED
        if old_y < y:
            dy = SPEED
        elif old_y > y:
            dy = -SPEED
        else:
            dy = 0

        # Move the box using the values set for dx and dy
        self.set_velocity(dx, dy)

    # Get the location and velocity of the box. If the box reaches its target x location, stop moving in the x. If the box reaches its target y location, stop moving in the y.
    def moved(self):
        (x, y) = self.pos()
        (dx, dy) = self.get_velocity()
        if self.target_x == x:
            dx = 0
        if self.target_y == y:
            dy = 0
        self.set_velocity(dx, dy)


picture = Picture()
picture.mainloop()
