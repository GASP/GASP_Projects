from gasp import *
import math

SCREENWIDTH=640
SCREENHEIGHT=480
CENTER_X = SCREENWIDTH / 2
CENTER_Y = SCREENHEIGHT / 2


class Planet(Circle):
    RADIUS = 50
    
    def __init__(self):
        self.shape = Circle((CENTER_X, CENTER_Y), Planet.RADIUS, 
                        True, (100,100,100))
        self.pos = (CENTER_X, CENTER_Y)
    
    def hit(self, what_hit_me):
        pass 

class KeepsToScreen:
    def keep_to_screen(self):
        oldpos = (x,y) = self.pos

        if x > SCREENWIDTH:
            x = 0
        if x < 0:
            x = SCREENWIDTH
        if y > SCREENHEIGHT:
            y = 0
        if y < 0:
            y = SCREENHEIGHT

        if (x,y) != oldpos:
            self.pos = (x,y)
            
class FeelsGravity:
    STRENGTH = 2000
    
    def gravitate (self):
        (x,y) = self.pos
        dist_x = CENTER_X - x
        dist_y = CENTER_Y - y
        dist_cubed = math.sqrt (dist_x * dist_x + dist_y * dist_y) ** 3
        strength = FeelsGravity.STRENGTH / dist_cubed
        a = (dist_x * strength, dist_y * strength)
                
        v = self.get_velocity ()
        v = add_vectors (v, a)
        self.set_velocity (v)

class FuelBar(Polygon):
    THICKNESS = 4
    INIT_POINTS = [(0,0),
                   (SCREENWIDTH,0),
                   (SCREENWIDTH,THICKNESS),
                   (0,THICKNESS)]
    num_bars = 2
    
    def __init__(self, y, start_fuel, color):
        self.conversion_factor = float(SCREENWIDTH)/start_fuel
                                         
        self.y_pos = FuelBar.num_bars * FuelBar.THICKNESS + 1
        FuelBar.num_bars = FuelBar.num_bars + 1
        self.color = color
        self.shape = Polygon(FuelBar.INIT_POINTS, False, self.color)
        move_to(self.shape, (SCREENWIDTH/2, self.y_pos))
        print self.color

        
    def update (self, fuel_level):
        length = self.conversion_factor * fuel_level
        remove_from_screen(self.shape)
        new_points = [(0,0),
                      (length,0),
                      (length,FuelBar.THICKNESS),
                      (0, FuelBar.THICKNESS)]
        self.shape = Polygon(new_points, False, self.color)
        move_to(self.shape, (length/2,self.y_pos))
        print self.color
class Bullet(Circle, FeelsGravity, KeepsToScreen):
    SPEED = 5
    TIME_TO_LIVE = 50
    SIZE = 2
    bullets = []
    
    def __init__ (self, ship_pos, ship_velocity, ship_angle):
        offset = Bullet.SIZE + 21
        offset_vec = angle_and_length (ship_angle, offset)
        self.pos = (self.x, self.y) = add_vectors (ship_pos, offset_vec)
        bullet_velocity = angle_and_length (ship_angle, Bullet.SPEED)
        vx = ship_velocity[0] / 6.0
        vy = ship_velocity[1] / 6.0
        (self.vx, self.vy) = add_vectors ((vx,vy), bullet_velocity)
        self.time_left = Bullet.TIME_TO_LIVE
        self.shape = Circle((self.x,self.y),Bullet.SIZE,filled = True)
        Bullet.bullets.append(self)

    def move(self):
        self.x, self.y = self.pos
        self.x += self.vx
        self.y += self.vy
        self.pos = (self.x,self.y)
        self.keep_to_screen()
        move_to(self.shape,self.pos)
        self.gravitate()
        self.time_left -= 1
        if self.time_left < 0:
            self.destroy()
        if distance_apart(self, my_planet) < Planet.RADIUS:
            self.hit(my_planet)
        if distance_apart(self, my_ship) < 10:
            self.hit(my_ship)
            my_ship.hit(self)
        if distance_apart(self, my_ship2) < 10:
            self.hit(my_ship2)
            my_ship2.hit(self)
        

    def destroy(self):
        try:
            remove_from_screen(self.shape)
            Bullet.bullets.remove(self)
        except:pass


    def hit (self, what_hit_me):
            self.destroy()
         
    def get_velocity(self):
        return (self.vx,self.vy)        

    def set_velocity(self, velocity):
        self.vx = velocity[0]
        self.vy = velocity[1]      

class Ship(Polygon, FeelsGravity, KeepsToScreen):
    LIST_POINTS = [(0,0),
                   (12,0),
                   (6,20)]
    THRUST = 1
    
    player1_controls = {"thrust": "w",
                        "fire": "s",
                        "right": "d",
                        "left": "a"}
                        
    player2_controls = {"thrust": "i",
                        "fire": "k",
                        "right": "l",
                        "left": "j"}
    

    AMMO = 40
    RELOAD = 60
    
    START_FUEL = 200
    
    
    def __init__(self, x, y, color, controls, fuelbar_y):
        self.shape = Polygon(Ship.LIST_POINTS, True, color)
        self.ammo = Ship.AMMO
        self.wait = Ship.RELOAD
        self.fuel_left = Ship.START_FUEL
        self.color = color
        self.fuelbar = FuelBar(fuelbar_y, self.fuel_left, self.color)
        self.target = None

        self.controls = controls
        self.x = x
        self.y = y
        self.pos = self.x,self.y
        self.move_x = 0
        self.move_y = 0
        self.angle = 0
        self.game_over = 0
        move_to(self.shape, (x,y))
    
    def move(self):
        self.x,self.y = self.pos
        keys = keys_pressed()
        if self.controls["left"] in keys:
            self.angle += 4
            rotate_by(self.shape,-4)
        if self.controls["right"] in keys:
            self.angle -= 4
            rotate_by(self.shape,4)
        if self.controls["fire"] in keys and self.ammo > 0:
            self.fire()
            self.ammo -= 1
        if self.controls["thrust"] in keys and self.fuel_left > 0:
            self.thrust()
            self.fuel_left -= 1
            self.fuelbar.update(self.fuel_left)
        self.x += self.move_x/10.0
        self.y += self.move_y/10.0
        self.pos = self.x,self.y
        self.gravitate()
        if self.ammo <= 0 and self.wait > 0:
            self.wait -= 1
            if self.wait <= 0:
                self.ammo = Ship.AMMO
                self.wait = Ship.RELOAD
        self.keep_to_screen()
        move_to(self.shape, self.pos)
        
        if distance_apart(self, my_planet) < Planet.RADIUS:
            self.hit(my_planet)
        
        if distance_apart(self, self.target) < 10:
            self.hit(self.target)
    
    def thrust (self):
        change_in_v = angle_and_length(self.angle, Ship.THRUST)
        v = self.get_velocity()
        new_v = add_vectors(v, change_in_v)
        self.set_velocity(new_v)

    def get_velocity(self):
        return (self.move_x,self.move_y)

    def set_velocity(self, velocity):
        self.move_x = velocity[0]
        self.move_y = velocity[1]   
            
            
        
    def fire(self):
        Bullet(self.pos, self.get_velocity(), self.angle)
        
    def hit(self, what_hit_me):
        if isinstance(what_hit_me, Planet):
            self.lost_game()
        
        elif isinstance(what_hit_me, Bullet):
            self.lost_game()
        
        elif isinstance(what_hit_me, Ship):
            self.drawn_game() 
        
    
    def lost_game(self):
        if not self.game_over:
            self.game_over = 1
            Text("You Lost", (SCREENWIDTH/8, SCREENHEIGHT/8), self.color, 50)
            sleep(3)
    
    def drawn_game(self):
        if not self.game_over:
            self.game_over = 1
            Text("DRAW", (SCREENWIDTH/8, SCREENHEIGHT/8), color.GREEN, 50)
            sleep(3)

def angle_and_length (angle, length):
    radian_angle = math.radians(angle)
    x = -(length * math.sin (radian_angle))
    y = length * math.cos (radian_angle)
    return (x, y)


def add_vectors (first, second):
    x = first [0] + second [0]
    y = first [1] + second [1]
    return (x,y)


def distance_apart(one, two):
    (x1, y1) = one.pos
    (x2, y2) = two.pos
    return math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )


begin_graphics(SCREENWIDTH,SCREENHEIGHT, "Space Wars")


colliding_objects = []
my_planet = Planet()
my_ship = Ship(200,200, color.RED, Ship.player1_controls, 100)
my_ship2 = Ship(500,200, color.BLUE, Ship.player2_controls, 200)
my_ship.target = my_ship2
my_ship2.target = my_ship

set_speed(90)
while not my_ship.game_over and not my_ship2.game_over:
    my_ship.move()
    my_ship2.move()
    for bullet in Bullet.bullets:
        bullet.move()
    update_when("next_tick")
