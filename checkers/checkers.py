# GASP games Checkers Sheet
# Written by Richard Martinez and Arron Birhanu

# gasp modules necessary to develop this game
from gasp import games
from gasp import boards
from gasp import color

# global variables, most of which define the board
BOX_SIZE = 40
MARGIN = 60
BOARD_SIZE = 8
WHITE = color.WHITE
BLACK = color.BLACK
BOARD_RED = color.RED
BOARD_BLACK = color.BLACK
COUNTER_SIZE = BOX_SIZE / 2 - 2
NEXT_PLAYER = {WHITE: BLACK,
               BLACK: WHITE}


class checkersGrid(boards.GameCell):  # grid cells established on the board
    def __init__(self, board, i, j):  # initializing gamecell attributes
        if i + j & 1 == 0:
            box_color = BOARD_RED
        else:
            box_color = BOARD_BLACK
        self.init_gamecell(board, i, j, fill_color=box_color)
        self.counter = None

    def center_pos(self):  # return center screen coords on this cell
        return self.screen_x + BOX_SIZE/2, self.screen_y + BOX_SIZE/2

    def new_counter(self, player):  # creating a new counter and its starting position
        self.counter = Counter(self.board, self.screen_x + BOX_SIZE / 2,
                               self.screen_y + BOX_SIZE / 2, player)

    def get_counter_color(self):  # returns the color, or more importantly which player, that owns the counter
        if self.counter is None:
            return

        return self.counter.get_color()

    def move_counter(self, i, j):  # move the counter to grid position i, j

        if not self.board.on_board(i, j):  # can't leave board
            return

        cell = self.board.grid[i][j]

        if self.counter is None or cell.counter is not None:
            # weed out illegal moves
            return

        # move the counter visually
        new_x, new_y = cell.center_pos()
        self.counter.move_to(new_x, new_y)

        # update who owns the counter internally
        cell.counter = self.counter
        self.counter = None

        return True  # return true if successfully moved

    def can_take(self, di, dj):  # return true if this counter is eligible to take another counter
        counter_color = self.get_counter_color()

        i, j = self.grid_x + di, self.grid_y + dj
        if self.board.on_board(i, j):
            return self.board.grid[i][j].get_counter_color() == NEXT_PLAYER[counter_color]

    def remove_counter(self):  # remove counter from board
        if self.counter is None:
            return
        self.counter.destroy()
        self.counter = None


class checkersBoard(boards.SingleBoard):  # the board in which two players operates pieces
    def __init__(self, box_size, margin, n_cols, n_rows):  # initializing singleboard's attributes
        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, box_size,
                              cursor_color=color.YELLOW)

        self.game_over = 0
        self.draw_all_outlines()
        self.enable_cursor(0, 0)
        self.set_background_color(color.DARKGREEN)

        self.white_move = {
            games.K_a: (-1, 1),
            games.K_d: (1, 1),
        }
        self.black_move = {
            games.K_a: (-1, -1),
            games.K_d: (1, -1),
        }

        self.num_whites = 0
        self.num_blacks = 0

        # create white counters
        for j in [0, 1, 2]:
            for i in [0, 2, 4, 6]:
                if j == 1:
                    i += 1
                self.grid[i][j].new_counter(WHITE)
                self.num_whites += 1

        # create black counters
        for j in [5, 6, 7]:
            for i in [1, 3, 5, 7]:
                if j == 6:
                    i -= 1
                self.grid[i][j].new_counter(BLACK)
                self.num_blacks += 1

        self.current_turn = WHITE  # white gets the first turn

        x = self.get_width() / 2
        y = MARGIN / 2
        self.text = games.Text(self, x, y, text="White's Turn", size=35, color=WHITE)

    def new_gamecell(self, i, j):  # initialize creation of squares on the board
        return checkersGrid(self, i, j)

        # grid is a list within the Gameboard class designed to
        # host pieces across coordinates shown as a list

    def toggle_turn(self):  # the player's turn
        self.current_turn = NEXT_PLAYER[self.current_turn]
        if self.current_turn == WHITE:
            self.text.set_text("White's Turn")
        elif self.current_turn == BLACK:
            self.text.set_text("Black's Turn")

    def keypress(self, key):  # manages the keys pressed
        if self.game_over:
            return

        self.handle_cursor(key)

        if key in self.white_move.keys():  # Either a or d
            counter_color = self.cursor.get_counter_color()

            if counter_color is None:  # Empty space
                return

            if counter_color != self.current_turn:  # Not your turn
                return

            if not self.cursor.counter.is_crowned:  # define normal counter movement

                if counter_color == WHITE:
                    di, dj = self.white_move[key]
                elif counter_color == BLACK:
                    di, dj = self.black_move[key]

            else:  # define crowned counter movement (must hold a direction)

                if self.is_pressed(games.K_w):  # Color doesn't matter for crowns
                    di, dj = self.black_move[key]
                elif self.is_pressed(games.K_s):
                    di, dj = self.white_move[key]
                else:
                    return  # Prevent error when not holding w or s

            if self.cursor.can_take(di, dj):  # Need to take that square
                i, j = self.cursor.grid_x, self.cursor.grid_y
                i1, j1 = i + di, j + dj
                i2, j2 = i + 2 * di, j + 2 * dj

                if not self.on_board(i2, j2) or self.grid[i2][j2].get_counter_color():
                    # return if off board, or space is occupied
                    return

                self.grid[i1][j1].remove_counter()
                if counter_color == WHITE:
                    self.num_blacks -= 1
                elif counter_color == BLACK:
                    self.num_whites -= 1

                i, j = i2, j2

            else:  # Nothing in the way
                i, j = self.cursor.grid_x + di, self.cursor.grid_y + dj

            if counter_color == WHITE and j == 7:  # where counter is crowned and what happens to counter
                self.cursor.counter.add_crown()
            elif counter_color == BLACK and j == 0:
                self.cursor.counter.add_crown()

            if self.cursor.move_counter(i, j):
                self.toggle_turn()

    def tick(self):  # keeps track of each move, determines when end game is
        if self.game_over:
            return

        if self.num_blacks <= 0:
            self.end_game(WHITE)
        elif self.num_whites <= 0:
            self.end_game(BLACK)

    def end_game(self, winner):  # kills game, determines who wins
        if winner == WHITE:
            self.text.set_text("White Wins!")
        elif winner == BLACK:
            self.text.set_text("Black Wins!")
        self.disable_cursor()
        self.game_over = 1


class Counter(boards.Container):  # creation of each counter (object)
    def __init__(self, board, x, y, player):  # initializing container's attributes
        # the visual order in which the crown and counter appear in
        self.init_container(["crown_circle", "player_circle"])
        self.crown_circle = games.Circle(board, x, y, radius=COUNTER_SIZE / 4,
                                         color=color.BLUE)
        self.player_circle = games.Circle(board, x, y, radius=COUNTER_SIZE,
                                          color=player)
        self.is_crowned = False

    def get_color(self):  # color of counter called
        return self.player_circle.get_color()

    def add_crown(self):  # attributes of the crown
        self.is_crowned = True
        self.crown_circle.raise_object()


checkers = checkersBoard(BOX_SIZE, MARGIN, BOARD_SIZE, BOARD_SIZE)
checkers.mainloop()
