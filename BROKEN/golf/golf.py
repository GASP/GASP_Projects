import math

from gasp import *


class Ball:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.size = 5
        self.shape = Circle(
            (self.x, self.y),
            self.size,
            color=color.WHITE,
            filled=True
        )
        self.direction = 0
        self.speed = 0
        self.struke = False
        self.striker = None

    def update(self, walls):
        if self.speed > 0:
            self.speed -= .015
        else:
            self.struke = False
            self.speed = 0
        dx = math.cos(math.radians(self.direction)) * self.speed
        dy = math.sin(math.radians(self.direction)) * self.speed
        self.check_walls(walls, dx, dy)
        if "b" in keys_pressed():
            difference = self.direction + 45
            self.direction = -45 - difference
        dx = math.cos(math.radians(self.direction)) * self.speed
        dy = math.sin(math.radians(self.direction)) * self.speed
        self.x += dx
        self.y += dy
        self.shape.move_to((self.x, self.y))

    def check_walls(self, walls, dx, dy):
        for wall in walls:
            if wall.check_bounce(self, dx, dy):
                return

    def strike(self):
        pos = mouse_position()
        distance = get_distance(pos, (self.x, self.y)) / 2.0
        if distance > 40:
            distance = 40
        direction = 90 - math.degrees(math.atan2(self.x-pos[0], self.y-pos[1]))
        d1 = direction + 4
        d2 = direction - 4
        x1 = math.cos(math.radians(d1)) * distance * 2 + self.x
        y1 = math.sin(math.radians(d1)) * distance * 2 + self.y
        x2 = math.cos(math.radians(d2)) * distance * 2 + self.x
        y2 = math.sin(math.radians(d2)) * distance * 2 + self.y
        points = [(self.x, self.y), (x1, y1), (x2, y2)]
        temp = Polygon(points, color=color.RED, filled=True)
        if self.striker:
            remove_from_screen(self.striker)
        self.striker = temp
        if mouse_buttons()["left"]:
            remove_from_screen(self.striker)
            self.hit(distance/8, direction)
            return True
        return False

    def hit(self, force, direction):
        self.direction = direction
        self.speed = force
        self.struke = True


class Wall:
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.change_x = self.start[0] - self.end[0]
        self.change_y = self.start[1] - self.end[1]
        self.direction = math.degrees(math.atan2(self.change_y, self.change_x))
        self.thickness = 5
        self.shape = Line(
            self.start,
            self.end,
            color=color.BROWN,
            thickness=self.thickness
        )

    def check_bounce(self, ball, dx, dy):
        shortest_distance = 100
        for i in range(500):
            pos = (
                self.start[0] - i * self.change_x / 500.0,
                self.start[1] - i * self.change_y / 500.0
            )
            distance = get_distance(pos, (ball.x+dx, ball.y+dy))
            if distance < shortest_distance:
                shortest_distance = distance
        if shortest_distance < ball.size + self.thickness / 2.0:
            difference = ball.direction - self.direction
            ball.direction = self.direction - difference
            return True
        return False


class Hole:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.size = 8
        self.shape = Circle((self.x, self.y), self.size, filled=True)

    def check_hole(self, ball):
        if get_distance((self.x, self.y), (ball.x, ball.y)) < self.size:
            if ball.speed < 1.5:
                move_to(ball.shape, (self.x, self.y))
                return True
            else:
                ball.speed -= ball.speed / 10
        return False


class Level:
    def __init__(self, num, level_data):
        self.hole_num = num
        self.hole = Hole(level_data[1])
        self.ball = Ball(level_data[2])
        self.par = level_data[3]
        self.walls = []
        for i in range(len(level_data[0])-1):
            self.walls.append(Wall(level_data[0][i], level_data[0][i+1]))
        self.walls.append(Wall(level_data[0][-1], level_data[0][0]))
        self.finished = False
        self.score = 0

    def play(self):
        while not self.finished:
            if self.ball.struke:
                self.ball.update(self.walls)
            else:
                self.score += self.ball.strike()
            self.finished = self.hole.check_hole(self.ball)
            update_when("next_tick")
        return self.score


class Player:
    def __init__(self, num):
        self.number = num+1
        self.name = Text_Entry(
            "What is your name player"+str(num+1)+"? ",
            (10, 200),
            color=color.WHITE,
            size=30
        )
        self.scores = []

    def get_score(self):
        score = 0
        for s in self.scores:
            score += s
        return score


class Game:
    def __init__(self, holes_path):
        hole_file = open(holes_path, 'r')
        self.holes = hole_file.readlines()
        self.num_holes = len(self.holes)
        self.players = []
        self.par = []

    def play(self):
        begin_graphics(background=color.GREEN)
        set_speed(80)
        self.num_players = int(Text_Entry(
            "How many players are there? ",
            (50, 200),
            color=color.WHITE,
            size=30)
        )
        for p in range(self.num_players):
            self.players.append(Player(p))
        for hole in range(self.num_holes):
            text = Text(
                "Hole: " + str(hole+1),
                (240, 200),
                size=30,
                color=color.WHITE
            )
            sleep(1.5)
            remove_from_screen(text)
            self.par.append(eval(self.holes[hole])[3])
            for player in self.players:
                text = Text(
                    "Your turn " + player.name,
                    (150, 200),
                    size=30,
                    color=color.WHITE
                )
                sleep(1.5)
                remove_from_screen(text)
                level = Level(hole+1, eval(self.holes[hole]))
                player.scores.append(level.play())
                sleep(1)
                clear_screen()
        self.show_scorecard()
        end_graphics()

    def show_scorecard(self):
        x = 30
        Line((x, 400), (x, 400-(80+40*len(self.players))), color=color.WHITE)
        t = Text("Hole", (x+5, 400-30), color=color.WHITE, size=20)
        Text("Par", (x+5, 400-70), color=color.WHITE, size=20)
        longest = get_text_length(t)
        for i in range(len(self.players)):
            lng = get_text_length(Text(
                self.players[i].name,
                (x+5, 290-40*i),
                color=color.WHITE,
                size=20)
            )
            if lng > longest:
                longest = lng
        x += longest + 10
        Line((x, 400), (x, 400-(80+40*len(self.players))), color=color.WHITE)
        for i in range(self.num_holes):
            x += 30
            Line(
                (x, 400),
                (x, 400-(80+40*len(self.players))),
                color=color.WHITE
            )
        Line((30, 400), (x+80, 400), color=color.WHITE)
        Line((30, 360), (x+80, 360), color=color.WHITE)
        Line((30, 320), (x+80, 320), color=color.WHITE)
        Line(
            (x+80, 400),
            (x+80, 400-(80+40*len(self.players))),
            color=color.WHITE
        )
        for i in range(len(self.players)):
            Line((30, 280-40*i), (x+80, 280-40*i), color=color.WHITE)
        for n in range(self.num_holes):
            Text(str(n+1), (50+longest+30*n, 370), color=color.WHITE, size=20)
            Text(
                str(self.par[n]),
                (50+longest+30*n, 330),
                color=color.WHITE,
                size=20
            )
            for p in range(len(self.players)):
                Text(
                    str(self.players[p].scores[n]),
                    (50+longest+30*n, 290-40*p),
                    color=color.WHITE,
                    size=20
                )
        Text("Total", (50+longest+30*(n+1), 370), color=color.WHITE, size=20)
        total = 0
        for t in self.par:
            total += t
        Text(
            str(total),
            (50+longest+30*(n+1), 330),
            color=color.WHITE,
            size=20
        )
        for p in range(len(self.players)):
            Text(
                str(self.players[p].get_score()),
                (50+longest+30*(n+1), 290-40*p),
                color=color.WHITE,
                size=20
            )
        while "enter" not in keys_pressed():
            pass


def get_distance(pos1, pos2):
    return math.sqrt((pos1[0]-pos2[0])**2+(pos1[1]-pos2[1])**2)


game = Game("holes.txt")
game.play()
