# GASP games Minecraft Crafting Quiz
# Written by Richard Martinez

# Do you know how to craft all the items in Minecraft? Prove it!
# Left click to select your item from the inventory and place it in the crafting grid.
# Right click to clear a cell in the crafting grid.
# Left click on the output item to submit your recipe
# Get a high score!

# Assets:
# https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/resource-pack-discussion/1251165-362-icons-minecraft-item-block-icons-download
# Some assets were edited by Richard

from gasp import games
from gasp import boards
from gasp import color
from gasp.utils import random_choice
import os
import json

# Global Variables
EMPTY_COLOR = color.LIGHTGRAY
BOX_SIZE = 60

CRAFTING_MARGIN = (130, 75)
INVENTORY_MARGIN = (130, 330)
OUTPUT_MARGIN = (550, 140)

BOARD_COLOR = color.DARKGRAY
OUTLINE_COLOR = color.DIMGRAY

# Arrow shape creation (only used to draw the arrow on screen)
ARROW_WIDTH = 100
ARROW_HEIGHT = 60
ARROW_THICKNESS = 20
ARROW_LENGTH = 60
ARROW_SHAPE = [
    (0, ARROW_HEIGHT/2 - ARROW_THICKNESS/2),
    (ARROW_LENGTH, ARROW_HEIGHT/2 - ARROW_THICKNESS/2),
    (ARROW_LENGTH, 0),
    (ARROW_WIDTH, ARROW_HEIGHT/2),
    (ARROW_LENGTH, ARROW_HEIGHT),
    (ARROW_LENGTH, ARROW_HEIGHT/2 + ARROW_THICKNESS/2),
    (0, ARROW_HEIGHT/2 + ARROW_THICKNESS/2)
]


class CraftingQuiz(games.Screen):
    def __init__(self):
        # Init Screen
        self.init_screen(width=800, height=600, title="Minecraft Crafting Quiz")
        self.set_background_color(EMPTY_COLOR)
        self.load_assets()

        # Init boards
        self.crafting_board = CraftingBoard(self)
        self.inventory = Inventory(self)
        self.output = Output(self)

        self.boards = [
            self.crafting_board,
            self.inventory,
            self.output
        ]

        # Draw the Arrow
        games.Polygon(self, x=380, y=135, shape=ARROW_SHAPE, color=BOARD_COLOR, outline=OUTLINE_COLOR)

        # Load in the recipes json file
        with open("recipes.json", "r") as f:
            self.recipes = json.load(f)

        self.craftables = list(self.recipes.keys())

        # Create a buffer to prevent repeat items
        self.buffer = []
        BUFFER_SIZE = 25
        for i in range(BUFFER_SIZE):
            self.buffer.append(None)

        # Draw score text
        x, y = self.output.cell_to_coords(0, -1)
        self.score = games.Text(self, x + BOX_SIZE/2, y - BOX_SIZE/2, "Score: 0", size=35, color=OUTLINE_COLOR)

        # Choose first craftable item
        self.new_craftable()

    def load_assets(self):
        # Load in all the assets
        self.assets = {}
        for filename in os.listdir("assets"):
            image = games.load_image(f"assets/{filename}")  # Grab the image

            # Fix Color Key Bug
            WHITE = (255, 255, 255, 255)
            BLACK = (0, 0, 0, 255)
            for px in range(image.get_width()):
                for py in range(image.get_height()):
                    if image.get_at((px, py)) == WHITE:
                        image.set_at((px, py), BLACK)
            image.set_colorkey(color.BLACK)

            image = games.set_resolution(image, x_resolution=BOX_SIZE-10, y_resolution=BOX_SIZE-10)

            name_str = filename.replace(".png", "")  # Remove the .png
            self.assets[name_str] = image

    def get_asset(self, name):
        return self.assets[name]

    def mouse_up(self, pos, button):
        if button == 0:  # Left click
            for board in self.boards:
                x, y = pos
                i, j = board.coords_to_cell(x, y)
                if board.on_board(i, j):
                    if board is self.crafting_board:
                        name = self.inventory.cursor.name
                        board.grid[i][j].set_image(name)
                    elif board is self.inventory:
                        board.move_cursor(i, j)
                    elif board is self.output:
                        # Prevent Mis-clicks
                        recipe = self.crafting_board.get_recipe()
                        recipe = self.uniform_recipe(recipe)
                        if len(recipe) == 0:
                            return

                        if self.check_recipe():
                            self.correct_answer()
                        else:
                            self.incorrect_answer()
        if button == 2:  # Right click
            x, y = pos
            i, j = self.crafting_board.coords_to_cell(x, y)
            if self.crafting_board.on_board(i, j):
                self.crafting_board.grid[i][j].clear_cell()

    def check_recipe(self):
        # Return True if the recipe is correct
        recipe = self.crafting_board.get_recipe()
        acceptable_recipes = self.get_acceptable_recipes()

        if len(acceptable_recipes) == 1 and acceptable_recipes[0][2][2] == 1:  # Shapeless
            recipe = self.uniform_recipe(recipe)
            acceptable_recipes = [self.uniform_recipe(acceptable_recipes[0])]

        return recipe in acceptable_recipes

    def correct_answer(self):
        # Get a new craftable item
        x, y = self.output.cell_to_coords(0, 1)
        games.Message(self, x + BOX_SIZE/2, y + BOX_SIZE/2, "Correct!", size=40, color=color.GREEN, lifetime=100)
        self.new_craftable()
        self.crafting_board.clear_board()

        # Add point
        score = int(self.score.get_text().split()[1])
        score += 1
        self.score.set_text(f"Score: {score}")

    def incorrect_answer(self):
        x, y = self.output.cell_to_coords(0, 1)
        games.Message(self, x + BOX_SIZE / 2, y + BOX_SIZE / 2, "Try Again", size=40, color=color.RED, lifetime=100)

    def uniform_recipe(self, recipe):
        # Return a uniformed recipe list for use in shapeless crafting
        single_list = []
        for row in recipe:
            for element in row:
                single_list.append(element)

        single_list = [i for i in single_list if i not in [None, 1]]  # Filter out None's and 1's
        single_list = sorted(single_list)  # Put in a uniform order (alphabetical)
        return single_list

    def tick(self):
        if self.mouse_buttons()[0]:  # Allow for holding left click
            x, y = self.mouse_position()
            i, j = self.crafting_board.coords_to_cell(x, y)
            if self.crafting_board.on_board(i, j):  # Only hold left click on crafting board
                self.mouse_up(self.mouse_position(), 0)

    def new_craftable(self):
        # Keep going to guarantee new item
        while True:
            craftable_name = random_choice(self.craftables)

            if craftable_name not in self.buffer:  # New item
                self.buffer = self.buffer[1:] + [craftable_name]  # Shift it over by one
                break

        self.output.set_image(craftable_name)
        self.output.text.set_text(craftable_name)

    def get_acceptable_recipes(self):
        name = self.output.get_name()
        return self.recipes[name]

    # Adding new recipes to JSON file
    # def _append_new_recipe(self, name):
    #     # Add new recipes to json
    #     try:
    #         _ = self.recipes[name]
    #     except KeyError:  # Doesn't exist yet
    #         self.recipes[name] = []
    #
    #     acceptable_recipes = self.recipes[name]  # self.get_acceptable_recipes()
    #     current_table = self.crafting_board.get_recipe()
    #     acceptable_recipes.append(current_table)
    #     self.recipes[name] = acceptable_recipes
    #
    #     # Write to json
    #     with open("recipes.json", "w") as f:
    #         json.dump(self.recipes, f, indent=2)
    #
    # def keypress(self, key):
    #     # For testing purposes
    #     if key == games.K_ESCAPE:
    #         for line in self.crafting_board.get_recipe():
    #             print(line)
    #         print()
    #         name = input("What item? ")
    #         self._append_new_recipe(name)
    #         self.crafting_board.clear_board()


class QuizBox(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.image = None
        self.name = ""

    def get_center_coords(self):
        x, y = self.board.cell_to_coords(self.grid_x, self.grid_y)
        return x + BOX_SIZE/2, y + BOX_SIZE/2

    def set_image(self, name):
        if name in [None, ""]:
            return

        if self.image:  # Clear cell if already occupied
            self.clear_cell()

        asset = self.screen.get_asset(name)
        x, y = self.get_center_coords()
        self.image = games.Sprite(self.screen, x, y, asset)
        self.name = name

    def clear_cell(self):
        if self.image is not None:
            self.image.destroy()
        self.image = None
        self.name = ""


class QuizBoard(boards.GameBoard):
    def __init__(self, screen, margin, n_cols, n_rows):
        self.init_quizboard(screen, margin, n_cols, n_rows)

    def init_quizboard(self, screen, margin, n_cols, n_rows):
        self.init_gameboard(screen, margin, n_cols, n_rows, BOX_SIZE, fill_color=BOARD_COLOR)
        self.draw_all_outlines()

    def new_gamecell(self, i, j):
        return QuizBox(self, i, j)


class CraftingBoard(QuizBoard):
    def __init__(self, screen, n_cols=3, n_rows=3):
        self.init_quizboard(screen, CRAFTING_MARGIN, n_cols, n_rows)

        x, y = self.cell_to_coords(1, 0)
        games.Text(screen, x + 10, y - 20, "Crafting", size=50, color=OUTLINE_COLOR)

    def get_recipe(self):
        recipe = [
            [None, None, None],
            [None, None, None],
            [None, None, None]
        ]

        for i in range(3):
            for j in range(3):
                name = self.grid[i][j].name
                if name == "":
                    name = None
                recipe[j][i] = name  # REVERSED

        return recipe

    def clear_board(self):
        self.map_grid(QuizBox.clear_cell)


class Inventory(QuizBoard):
    def __init__(self, screen, n_cols=9, n_rows=4):
        self.init_quizboard(screen, INVENTORY_MARGIN, n_cols, n_rows)
        self.enable_cursor(0, 3)

        x, y = self.cell_to_coords(1, 0)
        games.Text(screen, x + 20, y - 20, "Inventory", size=50, color=OUTLINE_COLOR)

        self.layout = [
            ["redstonedust", "redstonetorch", "flint", "string", "wheat", "apple", "melonslice", "blazerod", "blazepowder"],
            ["sugarcane", "paper", "book", "glowstonedust", "clay", "claybrick", "gunpowder", "feather", "enderpearl"],
            ["stick", "leather", "coal", "ironingot", "blockofiron", "goldingot", "goldnugget", "lapislazuli", "diamondgem"],
            ["woodenplank", "wood", "cobblestone", "stone", "sand", "glass", "obsidian", "wool", "brick"],
        ]

        self.draw_inventory()

    def draw_inventory(self):
        for row in self.layout:
            for position in row:
                i = row.index(position)
                j = self.layout.index(row)

                name = self.layout[j][i]  # j and i are REVERSED
                self.grid[i][j].set_image(name)


class Output(QuizBoard):
    def __init__(self, screen, n_cols=1, n_rows=1):
        self.init_quizboard(screen, OUTPUT_MARGIN, n_cols, n_rows)

        x, y = self.cell_to_coords(0, 0)
        self.text = games.Text(screen, x + BOX_SIZE/2, y - BOX_SIZE/2, "Output", size=40, color=OUTLINE_COLOR)

    def set_image(self, name):
        self.grid[0][0].set_image(name)

    def get_name(self):
        return self.grid[0][0].name


crafting_quiz = CraftingQuiz()
crafting_quiz.mainloop()
