# Chase by Benjamin Hyde
from gasp import *


class Wheel:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.shape = Circle(
            (x, y), 10, filled=True, thickness=2, color=color.RED
        )
        self.accerx = 0
        self.accery = 0
        self.velx = 0
        self.vely = 0

    def move_wheel(self):
        keys = keys_pressed()
        if "right" in keys:
            self.accerx = .065
            self.velx += self.accerx
        elif "left" in keys:
            self.accerx = -.065
            self.velx += self.accerx
        elif "up" in keys:
            self.accery = .065
            self.vely += self.accery
        elif "down" in keys:
            self.accery = -.065
            self.vely += self.accery
        self.vely -= .0098
        self.y += self.vely
        self.x += self.velx
        # print self.x, self.y
        move_to(self.shape, (self.x, self.y))

    def check_crash(self):
        if self.x <= 0:
            return True
        if self.x >= 800:
            return True
        if self.y <= 0:
            return True
        if self.y >= 800:
            return True
        return False

    def restart(self):
        self.x = 100
        self.y = 550
        move_to(self.shape, (self.x, self.y))


class Goal:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.shape = Circle(
            (x, y), z, filled=True, thickness=2, color=color.BLUE
        )

    def touched_by(self, arg):
        left = self.x - self.z < arg.x - 10 < self.x + self.z
        right = self.x - self.z < arg.x + 10 < self.x + self.z
        top = self.y - self.z < arg.y + 10 < self.y + self.z
        bottom = self.y - self.z < arg.y - 10 < self.y + self.z
        return (left and top) or (right and top) or (left and bottom) or \
               (right and bottom)

    def move(self, x, y):
        self.x = x
        self.y = y
        move_to(self.shape, (self.x, self.y))


finished = False

begin_graphics(800, 800, background=color.BLACK)

set_speed(100)
win = False
wheel = Wheel(100, 700)
goal = Goal(550, 550, 25)
level = 1
level_locations = [
    (550, 70), (70, 130), (400, 800), (700, 20), (750, 750), (40, 760),
    (150, 450), (600, 300), (760, 650), (400, 90), (800, 800), (800, 0),
    (0, 800), (400, 400), (400, 800), (700, 200), (200, 700), (600, 500),
    (700, 300), (200, 400), (300, 250), (600, 540), (100, 700)
]
time_limit = 6000  # 30 seconds
t = None

while not finished:
    # print goal.touched_by(wheel)
    if wheel.check_crash():
        finished = True

    time_limit -= 1
    if time_limit % 100 == 0:
        temp = Text(str(time_limit/100), (70, 70), size=12, color=color.WHITE)
        if t:
            remove_from_screen(t)
        t = temp

    if goal.touched_by(wheel):
        x, y = level_locations[level-1]
        goal.move(x, y)
        wheel.restart()
        level_text = Text(
            "level %d complete." % level,
            (220, 350),
            size=48,
            color=color.WHITE
        )
        level += 1
        sleep(1)
        remove_from_screen(level_text)

    if time_limit <= 0:
        finished = True
        Text(
            "Good job. You finished at level " + str(level) + ".",
            (150, 350),
            size=32,
            color=color.WHITE
        )
        sleep(3)
        end_graphics()

    wheel.move_wheel()
    # print "move"
    update_when("next_tick")


if not win:
    Text(
        "Game over. You lost at level " + str(level) + ".",
        (150, 350),
        size=32,
        color=color.WHITE
    )
sleep(3)
end_graphics()
