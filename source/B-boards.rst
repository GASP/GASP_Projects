..
    Copyright Gareth McCaughan and Jeffrey Elkner. All rights reserved.

    CONDITIONS:

    A "Transparent" form of a document means a machine-readable form,
    represented in a format whose specification is available to the general
    public, whose contents can be viewed and edited directly and
    straightforwardly with generic text editors or (for images composed of
    pixels) generic paint programs or (for drawings) some widely available
    drawing editor, and that is suitable for input to text formatters or for
    automatic translation to a variety of formats suitable for input to text
    formatters. A copy made in an otherwise Transparent file format whose
    markup has been designed to thwart or discourage subsequent modification
    by readers is not Transparent. A form that is not Transparent is
    called "Opaque".

    Examples of Transparent formats include LaTeX source and plain text.
    Examples of Opaque formats include PDF and Postscript.  Paper copies of
    a document are considered to be Opaque.

    Redistribution and use of this document in Transparent and Opaque
    forms, with or without modification, are permitted provided that the
    following conditions are met:

    - Redistributions of this document in Transparent form must retain
      the above copyright notice, this list of conditions and the following
      disclaimer.

    - Redistributions of this document in Opaque form must reproduce the
      above copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided with
      the distribution, and reproduce the above copyright notice in the
      Opaque document itself.

    - Neither the name of Scripture Union, nor LiveWires nor the names of
      its contributors may be used to endorse or promote products derived
      from this document without specific prior written permission.

    DISCLAIMER:

    THIS DOCUMENT IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS,
    CONTRIBUTORS OR SCRIPTURE UNION BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS DOCUMENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Graphics API for Students of Python: GASP - Boards Module
=========================================================

Introduction
-------------

This sheet is an extension of the GASP games module. See `Sheet H <H-games.html>`__ for more information on the games
module.

The boards module allows you to make board games in Python. Examples of board games include checkers, snake, and
tetris. All of these games rely on a grid-based game board where all the things in the game live. This, of course, is
complicated to do, so the boards module is here to take the reigns.

This sheet is more of a reference than an introduction. If you get confused,
ask a teacher for help. If you *are* a teacher or you are studying this on your own
and it still doesn't make any sense, feel free to send an email to
`jeff.elkner@gmail.com <mailto:jeff.elkner@gmail.com>`__ .

*Note:* All the information in `Sheet H <H-games.html>`__ is still accurate when using the boards module.


The Essentials
---------------

.. sourcecode:: python

    from gasp import games
    from gasp import boards

    # Make the board the main focus of the screen
    board = boards.SingleBoard(...)
    board.mainloop()

    # OR

    # Make the board just a part of a broader screen
    screen = games.Screen()
    board = boards.GameBoard(screen, ...)
    screen.mainloop()

These are the essentials. ``from gasp import boards`` imports the boards module.
To access any functions or classes in the boards module, call ``boards.*`` where
``*`` is the name of the thing you want to access.

*Note:* You usually want to access the ``games`` module as well when working with ``boards``.
See `Sheet H <H-games.html>`__ for information on how to use the games module.

Most of these classes are designed to be extended by subclassing. To avoid some of the syntactic
pain that usually attends this kind of design in Python, every class defines an extra method, which
takes exactly the same arguments as its ``__init__`` method. In a class called ``Foobar``, this method is
called ``init_foobar``.

The boards module is used in one of two ways:

1. Making the board the main focus of the screen (usually the easiest)
2. Making the board just a part of a broader screen (usually harder)

To make the board the main focus of the screen, use the ``SingleBoard`` class (or a subclass you create).
To make the board just a part of a broader screen, use the ``GameBoard`` class (or a subclass you create).


Boards Objects
----------------


GameCell
~~~~~~~~~~

``GameCell`` is a subclass of ``Polygon`` from the ``games`` module. It is intended to be subclassed itself to fit the
exact needs of a particular game, since it provides only fairly general methods and properties. Note that a
``GameCell`` is a static ``Polygon``. ``GameCell``’s shape is always a square.

Primarily, the ``GameCell`` class provides a number of useful properties, variables that belong to each ``GameCell``
separately. These properties are:

- ``grid_x``
    The column number of the cell in the grid, counting from zero as the leftmost column.
- ``grid_y``
    The row number of the cell in the grid, counting from zero as the top row.
- ``screen_x``
    The x-coordinate of the cell on the screen.
- ``screen_y``
    The y-coordinate of the cell on the screen.
- ``neighbors``
    A list of all the ``GameCell``'s which are next to this ``GameCell``. These lists are created by a method in the
    ``GameBoard`` class, which allows some control over exactly what “neighbor” should be taken to mean.
- ``direction``
    An array containing the ``GameCell`` which can be found in each direction from this ``GameCell``.
    This array is created by a method in the ``GameBoard`` class, which allows some control over exactly what
    “direction” should mean.

The boards module supplies a set of constants for using the ``direction`` array in a meaningful way. These are:

- ``boards.LEFT``
- ``boards.UP_LEFT``
- ``boards.UP``
- ``boards.UP_RIGHT``
- ``boards.RIGHT``
- ``boards.DOWN_RIGHT``
- ``boards.DOWN``
- ``boards.DOWN_LEFT``

You should not rely on these constants being particular values or in a particular order. Instead, the module provides
a number of useful functions to turn one direction into another, described later on.

In addition to the methods inherited from ``Polygon``, the following methods can be used on a ``GameCell``:

- ``init_gamecell(board, i, j, line_color=None, fill_color=None)``
    ``board`` is the ``GameBoard`` (see below) that the ``GameCell`` is on.

    ``(i, j)`` is the grid coordinates of the cell. In other words, ``i`` is the column number of the cell within the
    grid, while ``j`` is the row number. Cell ``(0, 0)``  is the top left-hand corner of the grid.

    ``line_color`` is the color of the line drawn between squares in the grid (i.e. the outline color of the
    ``Polygon``). If ``line_color`` is ``None``, it will default to the ``line_color`` of ``board``.

    ``fill_color`` is the color of the inside of each ``GameCell``. If ``fill_color`` is ``None``, it will default
    to the ``fill_color`` of ``board``.
- ``add_neighbor(neighbor)``
    You probably won’t need to call this method yourself, but if you do this is how. This method is called by the
    ``GameBoard`` class to create the neighbors list for a ``GameCell``. ``neighbor`` is a ``GameCell`` which is next to
    (in some sense) this cell.

    *Note:* this only sets up a *one-way relationship*. In other words, the instruction`` fred.add_neighbor(bill)`` tells
    ``fred`` that ``bill`` is his neighbor, but doesn't tell ``bill`` anything at all about ``fred``.
- ``add_direction(direction, cell)``
    Again, you probably won’t need to call this method yourself since it is called by the ``GameBoard`` class.
    It builds up the ``direction`` array for this ``GameCell``.

    ``direction`` is the direction under consideration.

    ``cell`` is the ``GameCell`` to be found in the ``direction``.

    *Note:* again, this method only establishes a connection *one-way*. The instruction
    ``lobby.add_direction(boards.LEFT, main_hall)`` tells the ``lobby`` that the ``main_hall`` is to its left,
    but doesn't let on to the ``main_hall`` that the ``lobby`` is near it at all.
- ``grid_coords()``
    Returns the grid coordinates of the ``GameCell`` as a tuple.
- ``screen_coords()``
    Returns the screen coordinats of the ``GameCell`` as a tuple.


GameBoard
~~~~~~~~~~~~

The ``GameBoard`` class is where you keep the ``GameCell``'s on your board. You will almost always want to subclass
``GameBoard`` (or more likely the ``SingleBoard`` class described below) to get exactly what you want for your game.

Each ``GameBoard`` contains a rectangular grid of ``GameCell``'s. Not all cells in the grid have to actually contain
``GameCell``'s, but all the cells are assumed to be square whether or not they are present.

The ``GameBoard`` class provides two useful properties (variables):

- ``grid``
    ``grid`` is a list of lists (see `Sheet A <A-lists.html>`__ if you’re not sure what that means) which contains every
    ``GameCell`` in the table. The cell at ``grid[i][j]`` is the cell which will appear in column ``i`` and row ``j``
    on the screen.
- ``cursor``
    ``cursor`` is an optional feature of the ``boards`` module. If it is switched on using ``enable_cursor`` (see below),
    ``cursor`` will contain the ``GameCell`` which is currently highlighted. All the board game worksheets use ``cursor``
    to control the game from the keyboard, moving the “cursor” around the board to select a particular cell that some
    other action should happen in.

``GameBoard`` provides the following methods:

- ``init_gameboard(screen, (x, y), n_cols, n_rows, box_size, line_color=color.BLACK, fill_color=color.LIGHTGRAY, cursor_color=color.RED)``
    ``screen`` is the ``Screen`` on which this board lives.

    ``(x,y)`` are the coordinates of the top left-hand corner of the board; under most circumstances these will be the
    coordinates of the top left-hand corner of the ``GameCell`` in the top left-hand corner of the grid.

    ``n_cols`` and ``n_rows`` are the number of columns and rows of ``GameCell``'s in the grid respectively.

    ``box_size`` is the height and width of each square in the grid.

    ``line_color`` is the color of the lines between or around each ``GameCell`` (in other words, the ``outline`` color
    of the ``Polygon``).

    ``fill_color`` is the color of the inside of each ``GameCell``.

    ``cursor_color`` is the color of the "cursor", the color which is used instead of the line color to highlight a
    particular ``GameCell`` if this feature of the class is used.
- ``new_gamecell(i, j)``
    You will probably want to override this method in any subclass that you create. This method is called automatically
    to create a new cell to fit into column ``i`` and row ``j`` of the grid. The version supplied with ``GameBoard``
    itself creates a ``GameCell``, which is almost certainly not what you want. Instead you should write a version
    which returns one of the subclasses of ``GameCell`` that you have made.
- ``create_neighbors(orthogonal_only=0)``
    This method must not be called before ``init_gameboard`` has been called. It scans through the grid, informing
    each ``GameCell`` of which other ``GameCell``'s are its neighbors.

    ``orthogonal_only`` is a flag controlling whether or not to include cells that are only diagonally adjacent to
    one another. If it is set to 0, cells on the diagonal are included; if it is 1, then only the cells directly up,
    right, down or left (*orthogonally adjacent*) of each cell are considered to be neighbors.
- ``create_directions(orthogonal_only=0, wrap=0)``
    This method must not be called before ``init_gameboard`` has been called. It scans through the grid, informing
    each ``GameCell`` of which other ``GameCell``'s lie in which directions from it.

    ``orthogonal_only`` is a flag controlling whether or not the diagonal directions are filled in. If it is 0 then the
    diagonals are considered, otherwise they are not.

    ``wrap`` is a flag which controls whether or not to “wrap” the board’s sense of direction. If ``wrap`` is 0, then
    there is considered to be nothing to the left of the leftmost column of cells, nothing above the top row of cells,
    and so on. If ``wrap`` is 1, then the rightmost column of cells are considered to be to the left of the leftmost
    column of cells, and the bottom row of cells are considered to be above the top row of cells, and vice versa.
    This allows a piece to move off the top of the board onto the bottom, for example. Basically, ``wrap`` controls
    whether or not the board allows for movement across the borders.
- ``keypress(key)``
    This method should be called whenever a key is pressed. This will happen automatically in any class that subclasses
    both ``GameBoard`` and ``Screen``, such as ``SingleBoard``. This method is used the same as the standard
    ``keypress`` in the ``Screen`` class from the ``games`` module.

    By default, only the cursor is handled. If you override this method, make sure you call ``handle_cursor`` somewhere
    in your method (probably at the beginning) if you still want the cursor to be handled.
- ``handle_cursor(key)``
    This is the handler method for the cursor. Call this somewhere in your ``keypress`` method if you want the cursor
    to be handled.

    Key codes that ``handle_cursor`` understands are held in the property ``key_movements``. This is a dictionary
    containing key codes matched to ``(dx, dy)`` pairs, where ``dx`` is the required change in the column number and
    ``dy`` is the change in row number for the cursor. The ``handle_cursor`` method does not allow these changes to
    take the cursor off the board, nor does it attempt to “wrap” the cursor by reintroducing it on the right if it
    tries to fall off the left of the grid. The key codes and movements set up by default are:

    - ``K_UP``: (0, -1)
    - ``K_DOWN``: (0, 1)
    - ``K_LEFT``: (-1, 0)
    - ``K_RIGHT``: (1, 0)

    ``key`` is the key to be handled. If ``handle_cursor`` is being called inside ``keypress``, ``key`` should be the
    same as the ``key`` you passed to ``keypress``.
- ``enable_cursor(i, j)``
    Switches on the cursor, placing it on the cell in column ``i`` and row ``j``. If the cursor already
    exists, it is simply moved to ``(i, j)``. If ``(i, j)`` is not a valid cell on the grid, an error will occur.
- ``disable_cursor()``
    Switches off the cursor, setting ``cursor`` to None and removing the cursor outline from the screen.
    It is safe to call this method if the cursor is already disabled; in that case, nothing will happen.
- ``move_cursor(i, j)``
    Moves the cursor to the cell at column ``i`` and row ``j`` on the grid. The cursor must already be enabled, and
    ``(i, j)`` must be a valid cell on the grid, otherwise an error will occur.
- ``cursor_moved()``
    This method is called automatically whenever the cursor is enabled, disabled (provided it was enabled at the time)
    or moved either explicitly in the program or by pressing keys. By default, it does nothing — you should override
    this method in your subclass if you want to be notified when the state of the cursor has changed.
- ``on_board(i, j)``
    Returns True if grid position ``(i, j)`` is really on the board, and False otherwise. By default, ``(i, j)`` is
    considered to be on the board if both ``i`` and ``j`` are zero or more, ``i`` is less than the maximum number of
    columns passed to ``init_gameboard`` and ``j`` is less than the maximum number of rows.

    ``on_board`` is used when creating the grid, and when setting up the ``direction`` and ``neighbors`` properties.
    You can override it in your subclass to produce a board that has holes in it (a Ludo board, for instance)
    automatically.
- ``map_grid(fn)``
    ``fn`` is a callable object (a function, usually) which is expected to take an entry from ``grid`` as a parameter.
    The ``map_grid`` method then calls ``fn`` on every cell on the board.
- ``draw_all_outlines()``
    Sets the board to draw the outlines of all the cells in the board. This is useful if you see a phantom trail of
    outlines behind an object. This also makes it easy for the player to tell exactly where the edge of the board is
    just by looking at it.

    Call this method either somewhere in your ``tick`` function (preferably towards the end), or in your board's
    ``__init__`` routine.
- ``limit_outlines(fn, true_color, false_color)``
    Sets the board to only draw the outlines of certain cells. ``fn`` is a callable object (a function, usually)
    which is expected to take an entry from ``grid`` as a parameter. For each box in the grid, if ``fn(box)`` returns
    True, the outline of ``box`` is set to ``true_color``. Otherwise, if ``fn(box)`` returns False, the outline of
    ``box`` will be set to ``false_color``. This is useful if you see a phantom trail of outlines behind an object.
    We recommend you set ``true_color`` to the outline color you want, and ``false_color`` to the background color.
    That way, the "false" cells will blend into the background.

    Call this method somewhere in your ``tick`` function. (preferably towards the end)
- ``cell_to_coords(i, j)``
    Returns the screen pixel coordinates of the top-left corner of the cell in column ``i`` and row ``j``.
- ``coords_to_cell(x, y)``
    Returns the grid coordinates (the column and the row) of the cell which contains the screen point ``(x, y)``.
    It is most often used for working out which ``GameCell`` the pointer was over when a mouse button was pressed or
    released.

    *Note:* the result is not necessarily a legal position for a cell, so check it with ``on_board``, before using it!


SingleBoard
~~~~~~~~~~~~~

The ``SingleBoard`` class is a straightforward subclass of ``GameBoard`` and ``Screen``. It provides a single grid of
``GameCell``'s that is the main focus of the window, which is the basis of most games. This is the class that you will
probably want to subclass for your own game, unless for some reason you want to have two or more boards in your window.

``SingleBoard`` provides one method in addition to those it inherits from ``Screen`` and ``GameBoard``.

- ``init_singleboard(margins, n_cols, n_rows, box_size, line_color=color.BLACK, fill_color=color.LIGHTGRAY, cursor_color=color.RED, title="GASP Games")``
    ``margins`` is a tuple giving the amount of space to leave on each side between the edge of the window and the edge
    of the grid. It comes in two alternatives: either ``(x, y)``, where ``x`` is the amount of space at the left and
    right and ``y`` the space above and below, or ``(l, t, r, b)``, where ``l`` is the left margin, ``r`` the right
    margin, ``t`` the space at the top and ``b`` the space at the bottom.

    ``n_cols`` and ``n_rows`` are the number of columns and rows respectively in the grid.

    ``box_size`` is the height and width of the grid squares.

    ``line_color`` is the color of the lines between or around each ``GameCell`` (in other words, the ``outline`` color
    of the ``Polygon``).

    ``fill_color`` is the color of the inside of each ``GameCell``.

    ``cursor_color`` is the color of the "cursor", the color which is used instead of the line color to highlight a
    particular ``GameCell`` if this feature of the class is used.

    ``title`` is the title of the screen.


Container
~~~~~~~~~~~

The ``Container`` class is a utility class that can be used to move grouped ``Object``'s around the screen.
Its purpose is to keep together, in the right order, any ``Object``'s that are supposed to stay together. For example,
if a counter is represented on the screen by a white ``Circle`` with a small red ``Circle`` in the middle of it,
the ``Container`` class allows us to move the white ``Circle`` and have the red ``Circle`` tag along automatically.

``Container`` overrides a number of the normal methods of an ``Object``. When you call a method on a ``Container``,
that same method is called on all of its ``contents`` (see below).

Here is an example ``Container`` class:

.. sourcecode:: python

    class MyCounter(boards.Container):
        def __init__(self, screen, x, y):
            self.init_container(['white_circle', 'red_circle'])

            self.white_circle = games.Circle(screen, x, y, radius=50, color.WHITE)
            self.red_circle = games.Circle(screen, x, y, radius=20, color.RED)

If you try moving this ``Container`` around, you will see that both the white and red ``Circle``'s tag along with each
other.

``Container`` provides one method for general use:

- ``init_container(contents)``
    ``contents`` is slightly unusual: it is a list of the names (as strings) of the objects which are to be contained.
    For example, suppose we have the white ``Circle`` containing a red ``Circle`` that we mentioned above, and set it
    up like this:

    .. sourcecode:: python

        self.white_circle = games.Circle(..., color.WHITE)
        self.red_circle = games.Circle(..., color.RED)

    Then we would have to call ``init_container(["white_circle", "red_circle"])`` to initialize it. Suppose our counter
    had to have a number on it as well:

    .. sourcecode:: python

        self.digit = games.Text(..., "1", ...)

    Then we would need to call ``init_container(["white_circle", "red_circle", "digit"])``. This would make sure that
    both the ``red_circle`` and the ``digit`` stayed on top of our ``white_circle``.
    Furthermore, ``digit`` would be on top of ``red_circle``!

    *Note:* The "contents" don't have to overlap the ``Container`` or each other, but they usually do. There is also
    nothing to stop the contents being a ``Container`` themselves! This can cause a chain reaction of ``Container``'s!

    *Note:* The ``init_container`` method must be called before the init routine for any other subclass
    (``init_circle`` in the example above), otherwise the library will produce a cryptic ``KeyError`` and quit.

    *Note:* When calling any ``Container`` method that is meant to return some data (``pos()`` or ``angle()`` for example),
    The values will always be pulled from the *first item* in its ``contents`` list. This shouldn't be a big deal if all
    the contents are overlapping each other, but if your first item is half way across the screen from everything else,
    you may be confused why the values ``Container`` returns appear to be wrong.

    As a side-effect, ``init_container`` will create ``self.red_circle`` and ``self.digit``, setting them to None.
    You should therefore not create your objects until after you have initialised your container!


Utility Functions
~~~~~~~~~~~~~~~~~~~

The ``boards`` module also supplies a number of useful functions specifically for playing with directions.
You should always use these functions rather than making any assumptions about what numbers mean what directions.
That way, if the library ever changes you won’t have to retype everything!

- ``turn_45_clockwise(direction)``
    This function returns the direction 45 degrees clockwise of ``direction``. In other words, if you call it with
    ``boards.UP``, it will reply with ``boards.UP_RIGHT``.

    The remaining functions all follow the same naming scheme, apart from the last one.
- ``turn_45_anticlockwise(direction)``
- ``turn_90_clockwise(direction)``
- ``turn_90_anticlockwise(direction)``
- ``turn_180(direction)``
- ``random_direction(orthogonal_only=0)``
    To avoid the need for you to write lots of tedious ``if``'s or cunning look-up code, this function returns a random
    direction. If ``orthogonal_only`` is 0, all 8 directions are equally likely to be selected; otherwise only the
    orthogonal directions ``(UP, DOWN, LEFT and RIGHT)`` will be chosen from.
