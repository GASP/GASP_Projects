from gasp import*
import math

global score_display, Score, lives, lives_display
enemies = []
bullets = []
Score = 0
btimer = 0
score_display = None
lives=3
lives_display = None
class Enemy():
    def __init__(self):  
        self.x = 0
        self.y = 0
        self.shape = 0
        self.num = 20
        
        

class Player():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.shape = 0
class Bullet():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.shape = 0
    
    
    
def place_enemies():
    global enemies
    enemy = Enemy()
    if len(enemies) > 0:
        if enemies[-1].y < 38.7:
            enemy.x = random_between(0, 40)
            enemy.y = 42
            enemy.shape = Polygon(((10*enemy.x-2, 10*enemy.y), (10*enemy.x+5, 10*enemy.y+5), (10*enemy.x+12, 10*enemy.y), (10*enemy.x+7, 10*enemy.y+8), (10*enemy.x+12, 10*enemy.y+12), (10*enemy.x+7, 10*enemy.y+12), (10*enemy.x+5, 10*enemy.y+17), (10*enemy.x+3, 10*enemy.y+12), (10*enemy.x-2, 10*enemy.y+12), (10*enemy.x+2, 10*enemy.y+7)), color=color.RED)  #Circle((enemy.x*10, enemy.y*10), 10, color= color.WHITE) 
            enemies.append(enemy)
    if len(enemies) == 0:
            enemy.x = random_between(0, 40)
            enemy.y = 42
            enemy.shape = Polygon(((10*enemy.x-2, 10*enemy.y), (10*enemy.x+5, 10*enemy.y+5), (10*enemy.x+12, 10*enemy.y), (10*enemy.x+7, 10*enemy.y+8), (10*enemy.x+12, 10*enemy.y+12), (10*enemy.x+7, 10*enemy.y+12), (10*enemy.x+5, 10*enemy.y+17), (10*enemy.x+3, 10*enemy.y+12), (10*enemy.x-2, 10*enemy.y+12
            ), (10*enemy.x+2, 10*enemy.y+7)), color=color.RED)
            enemies.append(enemy)
    
def move_enemies():
    global enemies
    for enemy in enemies:
        enemy.y -= .05
        move_to(enemy.shape, (enemy.x*10, enemy.y*10))
def remove_objects():
    global enemies, bullets
    bullet=Bullet()
    for enemy in enemies:
        if enemy.y <0:
            enemies.remove(enemy)
            remove_from_screen(enemy.shape)
    for bullet in bullets:
        if bullet.y > 400:
            bullets.remove(bullet)
            remove_from_screen(bullet.shape)
        
    

def place_player():
    global player
    player = Player()
    player.x = 190
    player.y = 50
    player.shape = Image('/home/aflecken/gasp-games/avoider/Spaceship.png', (player.x, player.y), width=50, height=50) 
    

def move_player():
    global player, bullets, btimer
    keys = keys_pressed()
    
    if 'a' in keys and player.x > 10:
        player.x -= 1
    elif 'd' in keys and player.x < 390:
        player.x += 1
    elif ' ' in keys and len(bullets) < 5 and btimer == 0:
        btimer = 1
        bullet=Bullet()
        bullet.x = player.x
        bullet.y = player.y
        bullet.shape = Circle((bullet.x, bullet.y), 3, filled = True, color=color.DODGERBLUE)
        bullets.append(bullet)
    if btimer == 100:
        btimer = 0
    elif btimer != 0:
        btimer += 1
        
    move_to(player.shape, (player.x, player.y))

def distance(x1, y1, x2, y2):
    return ((x2 - x1)**2 + (y2 - y1)**2)**0.5
    
def collided():
    global player, enemies, bullets
    for enemy in enemies:
        d = distance(player.x, player.y, enemy.x*10, enemy.y*10)
        if d <= 12:
            return True
            lives -= 1
    return False

def bullet_hit():
    global bullets, enemies
    for enemy in enemies:
        for bullet in bullets:
            d = distance(bullet.x, bullet.y, enemy.x*10, enemy.y*10)
            if d <= 9:
                enemies.remove(enemy)
                bullets.remove(bullet)
                remove_from_screen(enemy.shape)
                remove_from_screen(bullet.shape)
                

def update_score():
    global Score, score_display
    if score_display: remove_from_screen(score_display)
    Score += 1
    score_display = Text(str(Score) + " pts", (0, 0), size=12, color=color.WHITE)
def fire():
    global bullets, player, Score
    for bullet in bullets:
        bullet.y += 1
        move_to(bullet.shape, (bullet.x, bullet.y))
        
        
        
        
        
        
    
def display_lives():
    global lives, lives_display
    if lives_display: remove_from_screen(lives_display)
    if finished == True:
            lives-=1
begin_graphics(width = 400, height = 400, title = "Avoider", background = color.BLACK)
set_speed(300)

finished = False


Text('Avoider', (125,300), size = 48, color = color.WHITE)
Text('Programmed by Andrew Fleckenstein', (120, 275), size = 12, color = color.WHITE)
Text('Use the a and d keys to move', (120, 250), size = 12, color=color.WHITE)
Text('Press Space to Fire', (120, 225), size = 12, color=color.WHITE)
Text('Avoid the falling stars', (120, 200), size = 12, color=color.WHITE)
Text('They will fall faster as you keep playing', (120, 175), size = 12, color=color.WHITE)
Text('Press any key to continue', (120, 150), size = 12, color=color.WHITE)
lives_display = Text("Lives: " + str(lives), (300,0), size=12, color=color.WHITE)
update_when('key_pressed')



while True:
    finished = False
    enemies = []
    bullets = []
    Score = 0
    btimer = 0
    clear_screen()
    place_player()

    
    while not finished:

        place_enemies()
        move_enemies()
        move_player()
        fire()
        finished = collided()
        bullet_hit()
        remove_objects()
        update_score()
        display_lives()
        
        if Score >= 1000:
            set_speed(350)
        if Score >= 2000:
            set_speed(400)
        if Score >= 3000:
            set_speed(450)
        if Score >= 4000:
            set_speed(500)
        if Score >= 5000:
            set_speed(550)

        update_when('next_tick')   
    Text('You have ' + str(lives) +" Lives Left", (100, 200), size=26, color=color.WHITE)
    Text('Your Score was ' + str(Score), (125, 150), size=24, color=color.WHITE)
    sleep(1)
    if lives > 0:
        Text('Press R to restart, press any other key to quit.', (125, 125), size=12, color=color.WHITE)
    else:
        Text("You Lose.", (125, 125), size=24, color=color.RED)
        sleep(2)
        break
    keys = update_when('key_pressed')
    if 'r' in keys and lives > 0:
        pass
    else:
        break



end_graphics() 
