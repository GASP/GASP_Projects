# GASP games Tetris Sheet
# Written by Liam Brown

# NOTE: The name "Tetris" may be trademarked. Because of this, the name of this
# sheet may change in the future.

from gasp import games
from gasp import boards
from gasp import color
import random

BOX_SIZE = 30
WIDTH = 8
HEIGHT = 16
MARGIN = 2 * BOX_SIZE
INTERVAL = 40
TITLE_SIZE = 32
SCORE_SIZE = 24
EMPTY_COLOR = color.LIGHTGRAY


class TetrisBox(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.set_color(EMPTY_COLOR)
        self.full = 0

    def copy_from_above(self):
        self.set_color(self.direction[boards.UP].get_color())
        self.full = self.direction[boards.UP].full

    def clear_contents(self):
        self.set_color(EMPTY_COLOR)
        self.full = 0


class TetrisBoard(boards.SingleBoard):
    def __init__(self, interval):
        # Init Tetris Board
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.set_background_color(EMPTY_COLOR)
        self.interval = interval
        self.tick_count = interval
        self.game_over = 0
        self.create_directions(orthogonal_only=1)

        # Map the shapes to their colors
        self.BLOCK_COLORS = {
            SquareShape: color.YELLOW,
            LineShape: color.AQUA,
            ZShape: color.RED,
            ReverseZShape: color.LIME,
            TShape: color.PURPLE,
            LShape: color.ORANGE,
            ReverseLShape: color.BLUE
        }

        # First shape
        self.new_shape()

        # Show messages on screen
        tx, ty = self.cell_to_coords(WIDTH / 2, -1)
        self.status_message = games.Text(self, tx, ty + BOX_SIZE / 2,
                                         "T E T R I S", TITLE_SIZE, color.WHITE)
        tx, ty = self.cell_to_coords(WIDTH / 2, HEIGHT)
        self.score_message = games.Text(self, tx, ty + BOX_SIZE / 2,
                                        "Score: 0", SCORE_SIZE, color.WHITE)
        self.score = 0

    def tick(self):
        if self.game_over:
            return

        # Make shapes fall according to interval
        self.tick_count = self.tick_count - 1
        if self.tick_count == 0:
            self.tick_count = self.interval
            self.shape.drop()

        # Draw all outlines of the board
        self.draw_all_outlines()

    def new_shape(self):
        # Create a new shape at random
        self.shape = random.choice([SquareShape,
                                    LineShape,
                                    ZShape,
                                    ReverseZShape,
                                    TShape,
                                    LShape,
                                    ReverseLShape])(self)

    def new_gamecell(self, i, j):
        return TetrisBox(self, i, j)

    def end_game(self):
        self.game_over = 1
        self.status_message.set_text("GAME OVER")
        self.status_message.set_color(color.RED)

    def keypress(self, key):
        if self.game_over:
            return

        if key == boards.K_LEFT:
            self.shape.move(boards.LEFT)
        elif key == boards.K_RIGHT:
            self.shape.move(boards.RIGHT)
        elif key == games.K_SPACE:
            self.shape.plummet()
        elif key == games.K_RETURN:
            self.shape.rotate()

    def compress(self):
        # Clear a full row
        for j in range(HEIGHT):
            for i in range(WIDTH):
                if not self.grid[i][j].full:
                    break
            else:
                for k in range(j, 0, -1):
                    for i in range(WIDTH):
                        self.grid[i][k].copy_from_above()
                for i in range(WIDTH):
                    self.grid[i][0].clear_contents()
                self.score = self.score + 10
                self.score_message.set_text("Score: " + str(self.score))


class Shape:
    def __init__(self, board):
        self.init_shape(board)

    def init_shape(self, board):
        self.board = board
        self.color = self.board.BLOCK_COLORS[type(self)]

    def plummet(self):
        # Keep dropping until it hits something
        while not self.drop():
            pass

    def place(self):
        for box in self.boxes:
            box.set_color(self.color)
            if box.full:  # Reached the top of the screen
                self.board.end_game()

    def drop(self):
        new_boxes = []
        for box in self.boxes:
            new_boxes.append(box.direction[boards.DOWN])

        if not self.try_place(new_boxes):
            for box in self.boxes:
                box.full = 1
            self.board.compress()
            self.board.new_shape()
            return 1
        return 0

    def move(self, direction):
        new_boxes = []
        for box in self.boxes:
            new_boxes.append(box.direction[direction])
        self.try_place(new_boxes)

    def try_place(self, new_boxes):
        for box in new_boxes:
            if box is None or box.full:
                return 0
        for box in self.boxes:
            box.set_color(EMPTY_COLOR)
        self.boxes = new_boxes
        self.place()
        return 1


class SquareShape(Shape):
    # 00
    # 00
    def __init__(self, board):
        self.init_shape(board)
        self.boxes = [board.grid[WIDTH // 2 - 1][0],
                      board.grid[WIDTH // 2][0],
                      board.grid[WIDTH // 2 - 1][1],
                      board.grid[WIDTH // 2][1]]
        self.place()

    def rotate(self):
        pass  # Since it is a square, rotating it does nothing


class LineShape(Shape):
    # 0000
    def __init__(self, board):
        self.init_shape(board)
        self.vertical = random.randint(0, 1)
        if self.vertical:
            self.boxes = [board.grid[WIDTH // 2 - 1][0],
                          board.grid[WIDTH // 2 - 1][1],
                          board.grid[WIDTH // 2 - 1][2],
                          board.grid[WIDTH // 2 - 1][3]]
        else:
            self.boxes = [board.grid[WIDTH // 2 - 2][0],
                          board.grid[WIDTH // 2 - 1][0],
                          board.grid[WIDTH // 2][0],
                          board.grid[WIDTH // 2 + 1][0]]
        self.place()

    def rotate(self):
        if self.vertical:
            new_boxes = [self.boxes[1].direction[boards.LEFT],
                         self.boxes[1],
                         self.boxes[1].direction[boards.RIGHT]]
            if new_boxes[2] is None:
                return
            new_boxes.append(new_boxes[2].direction[boards.RIGHT])
        else:
            new_boxes = [self.boxes[1].direction[boards.UP],
                         self.boxes[1],
                         self.boxes[1].direction[boards.DOWN]]
            if new_boxes[1] is None:
                return
            new_boxes.append(new_boxes[2].direction[boards.DOWN])
        if self.try_place(new_boxes):
            self.vertical = not self.vertical


class ZShape(Shape):
    # 00
    #  00
    def __init__(self, board):
        self.init_shape(board)
        self.vertical = random.randint(0, 1)
        if self.vertical:
            self.boxes = [board.grid[WIDTH // 2][0],
                          board.grid[WIDTH // 2 - 1][1],
                          board.grid[WIDTH // 2][1],
                          board.grid[WIDTH // 2 - 1][2]]
        else:
            self.boxes = [board.grid[WIDTH // 2 - 2][0],
                          board.grid[WIDTH // 2 - 1][0],
                          board.grid[WIDTH // 2 - 1][1],
                          board.grid[WIDTH // 2][1]]
        self.place()

    def rotate(self):
        if self.vertical:
            new_boxes = [self.boxes[1].direction[boards.LEFT],
                         self.boxes[1],
                         self.boxes[1].direction[boards.DOWN]]
            if new_boxes[2] is None:
                return
            new_boxes.append(new_boxes[2].direction[boards.RIGHT])
        else:
            new_boxes = [self.boxes[1].direction[boards.DOWN],
                         self.boxes[1],
                         self.boxes[1].direction[boards.RIGHT]]
            if new_boxes[2] is None:
                return
            new_boxes.append(new_boxes[2].direction[boards.UP])
        if self.try_place(new_boxes):
            self.vertical = not self.vertical


class ReverseZShape(Shape):
    #  00
    # 00
    def __init__(self, board):
        self.init_shape(board)
        self.vertical = random.randint(0, 1)
        if self.vertical:
            self.boxes = [board.grid[WIDTH // 2 - 1][0],
                          board.grid[WIDTH // 2 - 1][1],
                          board.grid[WIDTH // 2][1],
                          board.grid[WIDTH // 2][2]]
        else:
            self.boxes = [board.grid[WIDTH // 2][0],
                          board.grid[WIDTH // 2 - 1][0],
                          board.grid[WIDTH // 2 - 1][1],
                          board.grid[WIDTH // 2 - 2][1]]
        self.place()

    def rotate(self):
        if self.vertical:
            new_boxes = [self.boxes[1].direction[boards.RIGHT],
                         self.boxes[1],
                         self.boxes[1].direction[boards.DOWN]]
            if new_boxes[2] is None:
                return
            new_boxes.append(new_boxes[2].direction[boards.LEFT])
        else:
            new_boxes = [self.boxes[1].direction[boards.UP],
                         self.boxes[1],
                         self.boxes[1].direction[boards.RIGHT]]
            if new_boxes[2] is None:
                return
            new_boxes.append(new_boxes[2].direction[boards.DOWN])
        if self.try_place(new_boxes):
            self.vertical = not self.vertical


class TShape(Shape):
    # 000
    #  0
    def __init__(self, board):
        self.init_shape(board)
        self.direction = boards.random_direction(orthogonal_only=1)
        if self.direction == boards.DOWN:
            self.boxes = [board.grid[WIDTH // 2 - 1][0]]
        else:
            self.boxes = [board.grid[WIDTH // 2 - 1][1]]
        self.boxes.append(self.boxes[0].direction[self.direction])
        self.boxes.append(self.boxes[0].direction[
                              boards.turn_90_clockwise(self.direction)])
        self.boxes.append(self.boxes[0].direction[
                              boards.turn_90_anticlockwise(self.direction)])
        self.place()

    def rotate(self):
        new_boxes = [self.boxes[0]]
        new_boxes.append(new_boxes[0].direction[
                             boards.turn_90_clockwise(self.direction)])
        new_boxes.append(new_boxes[0].direction[
                             boards.turn_90_clockwise(
                                 boards.turn_90_clockwise(self.direction))])
        new_boxes.append(new_boxes[0].direction[self.direction])
        if self.try_place(new_boxes):
            self.direction = boards.turn_90_clockwise(self.direction)


class LShape(Shape):
    # 0
    # 0
    # 00
    def __init__(self, board):
        self.init_shape(board)
        self.direction = boards.random_direction(orthogonal_only=1)
        if self.direction == boards.UP:
            self.boxes = [board.grid[WIDTH // 2 - 1][2]]
        elif self.direction == boards.LEFT:
            self.boxes = [board.grid[WIDTH // 2 - 1][1]]
        else:
            self.boxes = [board.grid[WIDTH // 2 - 1][0]]
        self.boxes.append(self.boxes[0].direction[
                              boards.turn_90_clockwise(self.direction)])
        self.boxes.append(self.boxes[0].direction[self.direction])
        self.boxes.append(self.boxes[2].direction[self.direction])
        self.place()

    def rotate(self):
        direction = boards.turn_90_clockwise(self.direction)
        new_boxes = [self.boxes[0],
                     self.boxes[0].direction[
                         boards.turn_90_clockwise(direction)],
                     self.boxes[0].direction[direction]]
        if new_boxes[2] is None:
            return
        new_boxes.append(new_boxes[2].direction[direction])
        if self.try_place(new_boxes):
            self.direction = direction


class ReverseLShape(Shape):
    #  0
    #  0
    # 00
    def __init__(self, board):
        self.init_shape(board)
        self.direction = boards.random_direction(orthogonal_only=1)
        if self.direction == boards.UP:
            self.boxes = [board.grid[WIDTH // 2][2]]
        elif self.direction == boards.RIGHT:
            self.boxes = [board.grid[WIDTH // 2][1]]
        else:
            self.boxes = [board.grid[WIDTH // 2][0]]
        self.boxes.append(self.boxes[0].direction[
                              boards.turn_90_anticlockwise(self.direction)])
        self.boxes.append(self.boxes[0].direction[self.direction])
        self.boxes.append(self.boxes[2].direction[self.direction])
        self.place()

    def rotate(self):
        direction = boards.turn_90_clockwise(self.direction)
        new_boxes = [self.boxes[0],
                     self.boxes[0].direction[
                         boards.turn_90_anticlockwise(direction)],
                     self.boxes[0].direction[direction]]
        if new_boxes[2] is None:
            return
        new_boxes.append(new_boxes[2].direction[direction])
        if self.try_place(new_boxes):
            self.direction = direction


tetris = TetrisBoard(INTERVAL)
tetris.mainloop()
