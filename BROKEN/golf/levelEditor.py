from gasp import *
level = []
wall_points = []
hole_pos = (-1,-1)
ball_pos = (-1,-1)
hole_file = open("holes.txt", 'r')
holes = hole_file.readlines()
begin_graphics(background = color.GREEN)
running = True
state = "walls"
set_speed(40)
line = None
line2 = None
ball = None
hole = None
text= Text("Place the Walls", (0,0),color=color.WHITE,size=30)
while running:
    keys = keys_pressed()
    pos = mouse_position()
    if state == "walls":
        if len(wall_points) == 0:
            pos1 = pos
        else:
            pos1 = wall_points[-1]
            if len(wall_points) > 1:
                temp = Line(wall_points[0],pos,thickness=5,color=color.BROWN)
                if line2:
                    remove_from_screen(line2)
                line2 = temp
        temp = Line(pos1,pos,thickness=5,color=color.BROWN)
        if line:
            remove_from_screen(line)
        line = temp
        if mouse_buttons()["left"]:
            wall_points.append(pos)
            if len(wall_points) > 1:
                Line(pos,wall_points[-2],thickness=5,color=color.BROWN)
            sleep(.5)
        if "enter" in keys and len(wall_points) > 2:
            state = "ball"
            remove_from_screen(line)
            remove_from_screen(line2)
            Line(wall_points[0],wall_points[-1],thickness=5,color=color.BROWN)
            remove_from_screen(text)
            text= Text("Place the Ball", (0,0),color=color.WHITE,size = 30)
    if state == "ball":
        if not ball:
            ball = Circle(pos,5,filled = True,color=color.WHITE)
        move_to(ball,pos)
        if mouse_buttons()["left"]:
            ball_pos = pos
            state = "hole"
            remove_from_screen(text)
            text= Text("Place the Hole", (0,0),color=color.WHITE,size = 30)
            sleep(.5)
    if state == "hole":
        if not hole:
            hole = Circle(pos,5,filled = True)
        move_to(hole,pos)
        if mouse_buttons()["left"]:
            hole_pos = pos
            remove_from_screen(text)
            par = int(Text_Entry("What is Par? ", (200,200),color=color.WHITE,size=30))
            text= Text("Done", (0,0),color=color.WHITE,size = 30)
            sleep(2)
            level = [wall_points,hole_pos,ball_pos,par]
            holes.append(str(level)+"\n")
            hole_file = open("holes.txt", 'w')
            hole_file.writelines(holes)
            hole_file.close()
            end_graphics()
    update_when("next_tick")
