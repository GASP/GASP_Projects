from gasp import *
from math import *


def orbit(o, c, r):
    for i in range(360):
        ignore = move_to(o, (c[0]+r*cos(i*pi/180), c[1]+r*sin(i*pi/180)))
        update_when('next_tick')


begin_graphics()

set_speed(30)

c = Circle((0, 0), 15, color=color.BLUE)
orbit(c, (320, 240), 100)
remove_from_screen(c)

b = Box((0, 0), 20, 20, color=color.GREEN, filled=True)
orbit(b, (320, 240), 130)
remove_from_screen(b)

end_graphics()
